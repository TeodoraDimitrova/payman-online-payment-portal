# **PayMan: ASP.NET Core MVC Web App** <br />

## 1. Team members:
   - Diana Krumova;
   - Teodora Dimitrova. <br />
 
## 2. Topic: **"PayMan"** (Online payment portal) <br />

## 3. Technologies used:
 &bull; .NET Core 2.2 <br />
 &bull; ASP.NET Core 2.2 <br />
 &bull; Entity Framework Core 2.2 <br />
 &bull; SQL Server Express 17.9 <br />
 &bull; JavaScript / jQuery <br />
 &bull; HTML/CSS <br />
 
## 4.  Features: <br />

   * Online payment portal that supports services for 3 types of visitors: "Admins", "Users", "Guests".
    
   * Admins can create clients' profiles and add users and accounts to them.

   * Users can transfer money and manage accounts.
   
   * Guests are able to see just the log in form and a banner.

## 5. [Find us online: PayMan]
[Find us online: PayMan]: https://paymanweb2019061510403722.azurewebsites.net
Credentials: **LoggedUser** (username)/ **user123** (password)

## 6. Covers the following requirements: <br />

 *The application should have:*

 * Public part (accessible without authentication)
	Home page with login box and banner advertisement.
	The homepage should display a random banner ad from all banner ads registered by an administrator in the administrative module and a login section with username and password fields.

 * Private part that contains "Account dashboard" and "Transaction page" (available for registered users)
	Default page in private area after login is Account dashboard page.

	* Account dashboard page should present a list of all accounts to which the user has access.
	For each account in the list, following information should be presented: Nickname, Account number, Current balance.
	Additional options should be available:
	  - Rename account's nickname;
	  - Make payment – initiates payment from respective account;
	  - View transactions – Presents all transactions on the account at the Transactions page.
	If the user has more than one registered accounts, also a new section should appear on the page, with pie chart for all accounts with pie sizes proportional to the balance of each account.

	* Transactions page presents a list of all payments made or received by the user.
	For each transaction, following information should be presented: Payer account, Payer client name, Payee account, Payee client name, Description, Amount, Timestamp, Indication if this is an outgoing or incoming transaction, Status ("Sent" or "Saved")
	Transactions should be sorted by timestamp in descending order.
	User should be able to filter transactions by account. If the user is redirected to Transactions page from "View transactions" functionality for an account from Account dashboard page, then respective account should be preloaded in the account selection dropdown.
	Transactions should be presented in pages of 5 with a navigation row positioned below transactions list.
	On Transactions page also a Button/Link should be available to initiate a new payment.

		* Make payment functionality (private)
		In each form following fields should be presented: Payer account (dropdown to select), Payer client name (non editable, fills automatically), Payee account (autocomplete), Payee client name (non editable, fills automatically), Description, Amount.
		Make payments functionality can be initiated either from Account dashboard or from Transactions page.
		Two options should be available: “Save” and “Send”. 

 * Administrative part (available for administrators only)
	Administrative users should have a separate administrative site.
	The homepage should display a login section with username and password fields for administrators.
 
	User management
	Admins should be able to register clients, accounts and users with following functionality:
	* Add client, with following attributes: Name (limited to 35 symbols).
	* For each client –  add accounts, with following attributes: Account number (10 digit number), Account balance(initializes with balance of 10 000 BGN, Account nickname (initializes by default = account number)
	* For each client – add users, with following attributes: Name (limited to 35 symbols), Username (limited to 16 symbols), Password (limited to 32 symbols; to be stored as hash in the database).
	* For each user of the client the administrator selects the subset of accounts (from all accounts of the client) to which the user will have access.

	Banner management
	Admins should be able to define banners with ads to be presented to users on user site home page.
	* Add banner, with following attributes: Banner image (image with fixed size), Banner link, Banner validity period – the banner should be visualized to users only during this period
	* Remove banner


