﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PayMan.Data.Models;
using PayMan.Data.Utilities;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace PayMan.Data
{
    public class PayContext : DbContext
    {
        protected PayContext()
        {
        }

        public PayContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Admin> Admins { get; set; }

        public DbSet<Banner> Banners { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<Movement> Movements { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Status> Statuses { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        public DbSet<UserApp> Users { get; set; }

        public DbSet<UsersAccounts> UsersAccounts { get; set; }

        public DbSet<UsersClients> UsersClients { get; set; }

        private void LoadJson(ModelBuilder builder)
        {
            if (File.Exists(DataConstants.Admins))
            {
                var admins = JsonConvert.DeserializeObject<Admin[]>(File.ReadAllText(DataConstants.Admins));

                foreach (var admin in admins)
                {
                    byte[] passwordHash, passwordSalt;
                    HashPassword.Generate(admin.PasswordHash, out passwordHash, out passwordSalt);

                    admin.PasswordSalt = Convert.ToBase64String(passwordSalt);
                    admin.PasswordHash = Convert.ToBase64String(passwordHash);
                }

                builder.Entity<Admin>().HasData(admins);
            }

            if (File.Exists(DataConstants.Movements))
            {
                var statuses = JsonConvert.DeserializeObject<Movement[]>(File.ReadAllText(DataConstants.Movements));
                builder.Entity<Movement>().HasData(statuses);
            }

            if (File.Exists(DataConstants.Roles))
            {
                var roles = JsonConvert.DeserializeObject<Role[]>(File.ReadAllText(DataConstants.Roles));
                builder.Entity<Role>().HasData(roles);
            }

            if (File.Exists(DataConstants.Statuses))
            {
                var statuses = JsonConvert.DeserializeObject<Status[]>(File.ReadAllText(DataConstants.Statuses));
                builder.Entity<Status>().HasData(statuses);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            LoadJson(modelBuilder);

            modelBuilder.Entity<UsersAccounts>().HasKey(x => new { x.AccountId, x.UserId });
            modelBuilder.Entity<UsersClients>().HasKey(x => new { x.ClientId, x.UserId });

            modelBuilder.Entity<Transaction>()
                        .HasOne(x => x.SenderAccount)
                        .WithMany(x => x.SendersTransactions)
                        .HasForeignKey(x => x.SenderAccountId)
                        .HasPrincipalKey(x => x.AccountId)
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

            modelBuilder.Entity<Transaction>()
                        .HasOne(x => x.ReceiverAccount)
                        .WithMany(x => x.ReceiversTransactions)
                        .HasForeignKey(x => x.ReceiverAccountId)
                        .HasPrincipalKey(x => x.AccountId)
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
