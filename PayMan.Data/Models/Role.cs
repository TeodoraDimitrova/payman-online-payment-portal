﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PayMan.Data.Models
{
    public class Role
    {
        [Key]
        public int RoleId { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<UserApp> Users { get; set; }

        public ICollection<Admin> Admins { get; set; }
    }
}
