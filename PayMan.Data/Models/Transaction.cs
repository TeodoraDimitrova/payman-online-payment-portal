﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PayMan.Data.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }

        [Required]
        [StringLength(35)]
        public string Description { get; set; }

        [Range(0, Double.MaxValue)]
        [RegularExpression(@"^\d+.?\d{0,2}$")]
        public decimal Amount { get; set; }

        public DateTime TimeStamp { get; set; }

        public int StatusId { get; set; }

        public Status Status { get; set; }

        public int SenderAccountId { get; set; }

        [ForeignKey(nameof(SenderAccountId))]
        [InverseProperty("SendersTransactions")]
        public Account SenderAccount { get; set; }

        public int ReceiverAccountId { get; set; }

        [ForeignKey(nameof(ReceiverAccountId))]
        [InverseProperty("ReceiversTransactions")]
        public Account ReceiverAccount { get; set; }

        public int? MovementId { get; set; }

        [ForeignKey(nameof(MovementId))]
        [InverseProperty("Transactions")]
        public Movement Movement { get; set; }
    }
}
