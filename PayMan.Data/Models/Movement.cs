﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PayMan.Data.Models
{
    public class Movement
    {
        [Key]
        public int MovementId { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }
}
