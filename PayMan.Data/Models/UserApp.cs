﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PayMan.Data.Models
{
    public class UserApp
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [StringLength(35)]
        public string FullName { get; set; }

        [Required]
        [StringLength(16)]
        public string UserName { get; set; }

        [Required]
        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

        [ForeignKey(nameof(Role))]
        public int RoleId { get; set; }

        public Role Role { get; set; }

        public ICollection<UsersAccounts> UsersAccounts { get; set; }

        public ICollection<UsersClients> UsersClients { get; set; }
    }
}
