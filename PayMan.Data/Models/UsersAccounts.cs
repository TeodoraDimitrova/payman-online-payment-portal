﻿namespace PayMan.Data.Models
{
    public class UsersAccounts
    {
        public int UserId { get; set; }

        public UserApp User { get; set; }

        public int AccountId { get; set; }

        public Account Account { get; set; }

        public string Nickname { get; set; }
    }
}
