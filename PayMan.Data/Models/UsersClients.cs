﻿namespace PayMan.Data.Models
{
    public class UsersClients
    {
        public int UserId { get; set; }

        public UserApp User { get; set; }

        public int ClientId { get; set; }

        public Client Client { get; set; }
    }
}
