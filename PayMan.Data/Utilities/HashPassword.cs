﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace PayMan.Data.Utilities
{
    public static class HashPassword
    {
        public static void Generate(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }
    }
}
