﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Data.Utilities
{
    public enum TransactionMovement
    {
        Incoming = 1,
        Outgoing = 2
    }
}
