﻿namespace PayMan.Data.Utilities
{
    public static class DataConstants
    {
        public const string Admins = @"..\PayMan.Data\Utilities\JsonFiles\Admins.json";
        public const string Movements = @"..\PayMan.Data\Utilities\JsonFiles\Movements.json";
        public const string Roles = @"..\PayMan.Data\Utilities\JsonFiles\Roles.json";
        public const string Statuses = @"..\PayMan.Data\Utilities\JsonFiles\Statuses.json";
    }
}
