﻿using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Services.Mappers;
using PayMan.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services
{
    public class AccountService : IAccountService
    {
        private readonly PayContext context;

        public AccountService(PayContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<AccountDTO> CreareAccountAsync(decimal balance, int clientId)
        {
            string accountNumber = RandomAccountNumber();

            while (await AccountNummberExistsAsync(accountNumber))
            {
                accountNumber = RandomAccountNumber();
            }

            var client = this.context.Clients.Find(clientId);
            Validator.IsNull(client, ServiceConstants.CanNotBeNull);

            var account = new Account
            {
                AccountNumber = accountNumber,
                Balance = balance,
                ClientId = clientId,
                Client = client
            };

            await this.context.AddAsync(account);
            await this.context.SaveChangesAsync();

            return account.ToDTO();
        }

        public async Task<UsersAccounts> AddAccountToUserAsync(int accountId, int userId)
        {
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.UserId == userId);

            Validator.IsNull(user, ServiceConstants.CanNotBeNull);

            var account = await this.context.Accounts.FirstOrDefaultAsync(acc => acc.AccountId == accountId);
            Validator.IsNull(account, ServiceConstants.CanNotBeNull);

            var clientId = account.ClientId;

            var userClient = await this.context.UsersClients
                .Where(u => u.UserId == userId && u.ClientId == clientId)
                .FirstOrDefaultAsync();

            Validator.IsNull(userClient, ServiceConstants.CanNotBeNull);

            var userAccounts = new UsersAccounts
            {
                UserId = user.UserId,
                User = user,
                Account = account,
                AccountId = account.AccountId,
                Nickname = account.AccountNumber
            };

            await this.context.UsersAccounts.AddAsync(userAccounts);
            await this.context.SaveChangesAsync();
            return userAccounts;
        }

        public async Task<UsersAccounts> RemoveAccountFromUserAsync(int accountId, int userId)
        {
            var userAccounts = await this.context.UsersAccounts
                .Include(u => u.User)
                .FirstOrDefaultAsync(u => u.UserId == userId && u.AccountId == accountId);

            this.context.UsersAccounts.Remove(userAccounts);
            await this.context.SaveChangesAsync();
            return userAccounts;
        }

        public async Task<UsersAccounts> EditNickNameAsync(int accountId, int userId, string nickName)
        {
            var account = await this.context.UsersAccounts
                .FirstOrDefaultAsync(acc => acc.AccountId == accountId && acc.UserId == userId);

            Validator.IsNull(account, ServiceConstants.CanNotBeNull);

            account.Nickname = nickName;

            this.context.UsersAccounts.Update(account);
            this.context.SaveChanges();

            return account;
        }

        public async Task<ICollection<Account>> GetClientAccountsAsync(int clientId)
        {
            var client = await this.context.Clients
                                    .Include(a => a.Accounts)
                                    .ThenInclude(ua => ua.UsersAccounts)
                                    .FirstOrDefaultAsync(c => c.ClientId == clientId);

            Validator.IsNull(client, ServiceConstants.CanNotBeNull);

            var accounts = client.Accounts;

            return accounts;
        }

        public async Task<ICollection<AccountDTO>> GetAccountsAsync(int userId)
        {
            var accounts = await this.context.UsersAccounts
                .Where(u => u.UserId == userId)
                .Include(x => x.Account)
                .ThenInclude(x => x.Client)
                .ToListAsync();

            var dtos = accounts.Select(x => x.Account.ToDTO()).ToList();

            return dtos;
        }

        public async Task<ICollection<UsersAccounts>> GetUserAccountsAsync(int userId)
        {
            var accounts = await this.context.UsersAccounts
                .Where(u => u.UserId == userId)
               .Include(acc => acc.Account)
               .ThenInclude(c => c.Client)              
               .ToListAsync();

            return accounts;
        }

        public async Task<ICollection<UsersAccounts>> GetAccountUsersAsync(int accountId)
        {
            var users = await this.context.UsersAccounts
                               .Where(acc => acc.AccountId == accountId)
                               .Include(u => u.User)
                               .ToListAsync();

            return users;
        }

        private async Task<bool> AccountNummberExistsAsync(string accountNumber)
        {
            return await context.Accounts.AnyAsync(acc => acc.AccountNumber == accountNumber);
        }

        private static string RandomAccountNumber()
        {
            var random = new Random();
            var number = new StringBuilder();

            for (int i = 0; i <= 9; i++)
            {
                number.Append((char)('0' + random.Next(10)));
            }

            return number.ToString();
        }
    }
}
