﻿using Dapperer;
using PayMan.Services.DTOs;
using PayMan.Services.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services.Contracts
{
    public interface ITransactionService
    {
        Task<TransactionDTO> SaveNewTransAsync(TransactionRequest request);

        Task<TransactionDTO> SendNewTransAsync(TransactionRequest request);

        Task<TransactionDTO> UpdateTransactionAsync(int id, TransactionRequest request);

        Task<TransactionDTO> SendSavedAsync(int id, TransactionRequest request);

        Task<TransactionDTO> SendAsync(int id);

        Task<TransactionDTO> GetTransactionAsync(int id);

        Task<int> SavedTransactionsCountAsync(FilteredTransactionsRequest request);

        Task<int> SentTransactionsCountAsync(FilteredTransactionsRequest request);

        Task<ICollection<TransactionDTO>> GetTransactionsAsync(int skip, int pageSize, FilteredTransactionsRequest request);

        Task<ICollection<TransactionDTO>> GetSavedTransactionsAsync(int skip, int pageSize, FilteredTransactionsRequest request);

        Task<ICollection<AccountDTO>> SenderAccountNumbersAsync(int userId);

        Task<ICollection<AccountDTO>> ReceiverAccountNumbersAsync(int userId);

        Task<ICollection<AccountDTO>> SearchAsync(string text);
    }
}
