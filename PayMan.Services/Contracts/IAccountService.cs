﻿using PayMan.Data.Models;
using PayMan.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services.Contracts
{
    public interface IAccountService
    {
        Task<AccountDTO> CreareAccountAsync(decimal balance, int clientId);

        Task<ICollection<Account>> GetClientAccountsAsync(int clientId);

        Task<ICollection<AccountDTO>> GetAccountsAsync(int userId);

        Task<ICollection<UsersAccounts>> GetAccountUsersAsync(int accountId);

        Task<UsersAccounts> AddAccountToUserAsync(int accountId, int userId);

        Task<UsersAccounts> RemoveAccountFromUserAsync(int accountId, int userId);

        Task<UsersAccounts> EditNickNameAsync(int accountId, int userId, string nickName);

        Task<ICollection<UsersAccounts>> GetUserAccountsAsync(int userId);
    }
}
