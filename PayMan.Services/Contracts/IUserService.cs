﻿using Microsoft.AspNetCore.Authentication;
using PayMan.Data.Models;
using PayMan.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services.Contracts
{
    public interface IUserService
    {

        Task<UserApp> CreateAsync(string userName, string name, string password);

        Task<ICollection<UserApp>> GetUsersAsync(int skip, int pageSize, string searchValue);

        Task<List<string>> GetUsersNameAsync(string term);

        Task <int> GetTotalUsersCountAsync();

        Task<bool> ExistUserAsync(string username);

        Task<UserApp> AuthenticateAsync(string username, string password);

        Task<Admin> AuthenticateAdminAsync(string username, string password);

        ClaimsPrincipal CreateClaimsPrincipal(UserApp user, string scheme);

        ClaimsPrincipal AdminClaimsPrincipal(Admin user, string scheme);

        AuthenticationProperties GetCookieOptions();

    }
}
