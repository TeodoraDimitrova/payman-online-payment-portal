﻿using Microsoft.AspNetCore.Http;
using PayMan.Services.DTOs;
using PayMan.Services.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services.Contracts
{
    public interface IBannerService
    {
        Task<BannerDTO> AddBannerAsync(string root, BannerRequest request);

        Task<BannerDTO> UpdateBannerAsync(int id, string root, BannerRequest request);

        Task DeleteBannerAsync(int id, string root);

        Task<BannerDTO> RandomBannerAsync();

        Task<BannerDTO> GetBannerAsync(int id);

        Task<ICollection<BannerDTO>> GetAllAsync();
    }
}
