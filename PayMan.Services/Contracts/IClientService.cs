﻿using PayMan.Data.Models;
using PayMan.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services.Contracts
{
    public interface IClientService
    {
        Task<ClientDTO> CreateClientAsync(string name);

        Task<IList<ClientDTO>> GetClientsAsync(int skip, int pageSize, string searchValue);

        int GetTotalClientsCount();

        Task<bool> ExistClientAsync(string name);

        Task<ClientDTO> GetClientByIdAsync(int id);

        Task<ICollection<UsersClients>> GetUsersClientsAsync(int clientId);

        Task<UserApp> RemoveUserAsync(int userId, int clientId);

        Task<UserApp> AddUserToClient(string userName, int clientId);



    }
}
