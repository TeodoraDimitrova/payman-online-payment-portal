﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.Utilities
{
    public static class ServiceConstants
    {
        //AccountService's constants

        //BannerService's constants
        public const string NoBanner = "Currently no banners are available.";

        //TransactionService's constants
        public const string TransactionNotFound = "The transaction that you are looking for never occured.";
        public const string InsufficientAmount = "Insufficient balance to complete the transaction.";
        public const string SameNumbers = "Sending and receiving accounts cannot have the same account number.";

        //UserService's constants
        public const string CanNotBeNull = "Value can not be null";

        //Other
        public const string InvalidInput = "Invalid input data.";
    }
}
