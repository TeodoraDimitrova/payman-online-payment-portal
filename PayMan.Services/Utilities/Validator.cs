﻿using PayMan.Services.Exceptions;
using PayMan.Services.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.Utilities
{
    public static class Validator
    {
        public static void IsNull(object obj, string message)
        {
            if (obj == null)
            {
                throw new UserException(message);
            }
        }

        public static void SufficientAmount(decimal balance, decimal amount)
        {
            if (amount > balance)
            {
                throw new UserException(ServiceConstants.InsufficientAmount);
            }
        }

        public static void AccountCheck(string first, string second)
        {
            if (first == second)
            {
                throw new UserException(ServiceConstants.SameNumbers);
            }
        }

        public static void TransactionRequest(TransactionRequest request, string message)
        {
            if (request == null || request.Description == null || request.Description.Length > 35 
                || request.Amount < 0 || request.ReceiverAccount.Length != 10)
            {
                throw new UserException(message);
            }
        }

        public static void Request(BannerRequest request, string message)
        {
            if (request == null || string.IsNullOrEmpty(request.URL)
                || request.StartValidity > request.EndValidity
                || request.StartValidity.Year.ToString().Length > 4
                || request.EndValidity.Year.ToString().Length > 4)
            {
                throw new UserException(message);
            }
        }
    }
}
