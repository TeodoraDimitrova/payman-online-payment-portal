﻿using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services
{
    public class ClientService:IClientService
    {
        private readonly PayContext context;

        public ClientService(PayContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<ClientDTO> CreateClientAsync(string name)
        {
            var client = await this.context.Clients.FirstOrDefaultAsync(x => x.Name == name);

            if (client == null)
            {
                client = new Client()
                {
                    Name = name
                };
            }

            await this.context.Clients.AddAsync(client);
            await this.context.SaveChangesAsync();

            return client.ToDTO();

        }

        public async Task<bool> ExistClientAsync(string name)
        {
            var isClientExist = await this.context.Clients.AnyAsync(x => x.Name == name);
            return isClientExist;
        }

        public async Task<IList<ClientDTO>> GetClientsAsync(int skip, int pageSize, string searchValue)
        {

            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                var client =  await this.context.Clients
                    .Where(a => a.Name.ToLower()
                    .StartsWith(searchValue))
                    .OrderBy(a => a.Name)
                    .Skip(skip)
                    .Take(pageSize)
                    .Select(c=>c.ToDTO())
                    .ToListAsync();
                return client;
            }
            else
            {
                var clients= await this.context.Clients
                    .OrderBy(a => a.Name)                    
                    .Skip(skip)
                    .Take(pageSize)
                    .Select(c => c.ToDTO())
                    .ToListAsync();

                return clients;
            }
        }

        public async Task<ClientDTO> GetClientByIdAsync(int id)
        {
            var client = await this.context.Clients
                .Include(u => u.UsersClients)
                     .ThenInclude(uc => uc.User)
                .FirstOrDefaultAsync(c => c.ClientId == id);

            return client.ToDTO();
        }

        public int GetTotalClientsCount()
        {
            return this.context.Clients.Count();
        }

        public async Task<ICollection<UsersClients>> GetUsersClientsAsync(int clientId)
        {
            var uc = await this.context.UsersClients
                .Include(x=>x.User)
                .Where(c => c.ClientId == clientId).ToListAsync();
            return uc;
        }

        public async Task<UserApp> RemoveUserAsync(int userId, int clientId)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.UserId == userId);

            var userClients = await this.context.UsersClients
                                      .Include(u=>u.User)
                                      .Where(u => u.UserId==userId&&u.ClientId==clientId)
                                      .FirstOrDefaultAsync();

            var userAccounts = this.context.UsersAccounts
                                .Where(u => u.UserId == userId && u.Account.ClientId==clientId);

            this.context.UsersAccounts.RemoveRange(userAccounts);
            this.context.UsersClients.Remove(userClients);
            await this.context.SaveChangesAsync();

            return user;
        }

        public async Task<UserApp> AddUserToClient(string userName, int clientId)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.UserName == userName);
            var client = await this.context.Clients.FirstOrDefaultAsync(c => c.ClientId == clientId);

            var userClients = new UsersClients
            {
                User = user,
                UserId = user.UserId,
                Client = client,
                ClientId = client.ClientId
            };

            await this.context.UsersClients.AddAsync(userClients);
            await this.context.SaveChangesAsync();

            return user;
        }
    }
}
