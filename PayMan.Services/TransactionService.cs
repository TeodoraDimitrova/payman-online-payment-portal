﻿using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Services.Mappers;
using PayMan.Services.Requests;
using PayMan.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly PayContext context;

        public TransactionService(PayContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<TransactionDTO> SaveNewTransAsync(TransactionRequest request)
        {
            Validator.TransactionRequest(request, ServiceConstants.InvalidInput);

            var senderAccount = await this.context.Accounts
                .Include(x => x.Client)
                .FirstOrDefaultAsync(x => x.AccountId == request.SenderAccountId && x.Client.Name == request.SenderClient);
            Validator.IsNull(senderAccount, ServiceConstants.InvalidInput);
            Validator.AccountCheck(senderAccount.AccountNumber, request.ReceiverAccount);

            var receiverAccount = await this.context.Accounts
                .Include(x => x.Client)
                .FirstOrDefaultAsync(x => x.AccountNumber == request.ReceiverAccount && x.Client.Name == request.ReceiverClient);
            Validator.IsNull(receiverAccount, ServiceConstants.InvalidInput);

            var transaction = new Transaction()
            {
                Description = request.Description,
                Amount = request.Amount,
                TimeStamp = DateTime.Now,
                StatusId = 2,
                SenderAccountId = senderAccount.AccountId,
                ReceiverAccountId = receiverAccount.AccountId
            };
            
            await this.context.Transactions.AddAsync(transaction);
            await this.context.SaveChangesAsync();

            return transaction.ToDTO();
        }

        public async Task<TransactionDTO> SendNewTransAsync(TransactionRequest request)
        {
                Validator.TransactionRequest(request, ServiceConstants.InvalidInput);

                var senderAccount = await this.context.Accounts
                    .Include(x => x.Client)
                    .FirstOrDefaultAsync(x => x.AccountId == request.SenderAccountId && x.Client.Name == request.SenderClient);
                Validator.IsNull(senderAccount, ServiceConstants.InvalidInput);
                Validator.AccountCheck(senderAccount.AccountNumber, request.ReceiverAccount);
                Validator.SufficientAmount(senderAccount.Balance, request.Amount);

                var receiverAccount = await this.context.Accounts
                    .Include(x => x.Client)
                    .FirstOrDefaultAsync(x => x.AccountNumber == request.ReceiverAccount && x.Client.Name == request.ReceiverClient);
                Validator.IsNull(receiverAccount, ServiceConstants.InvalidInput);

                var incomingTransaction = new Transaction()
                {
                    Description = request.Description,
                    Amount = request.Amount,
                    TimeStamp = DateTime.Now,
                    StatusId = 1,
                    SenderAccountId = senderAccount.AccountId,
                    ReceiverAccountId = receiverAccount.AccountId,
                    MovementId = 1
                };

                var outgoingTransactionransaction = new Transaction()
                {
                    Description = request.Description,
                    Amount = request.Amount,
                    TimeStamp = DateTime.Now,
                    StatusId = 1,
                    SenderAccountId = senderAccount.AccountId,
                    ReceiverAccountId = receiverAccount.AccountId,
                    MovementId = 2
                };

                senderAccount.Balance -= request.Amount;
                receiverAccount.Balance += request.Amount;

                this.context.Accounts.Update(senderAccount);
                this.context.Accounts.Update(receiverAccount);
                await this.context.Transactions.AddAsync(incomingTransaction);
                await this.context.Transactions.AddAsync(outgoingTransactionransaction);
                await this.context.SaveChangesAsync();

                return outgoingTransactionransaction.ToDTO();
        }

        public async Task<TransactionDTO> UpdateTransactionAsync(int id, TransactionRequest request)
        {
            Validator.TransactionRequest(request, ServiceConstants.InvalidInput);

            var transaction = await this.context.Transactions.FirstOrDefaultAsync(x => x.TransactionId == id);
            Validator.IsNull(transaction, ServiceConstants.InvalidInput);

            var senderAccount = await this.context.Accounts
                .Include(x => x.Client)
                .FirstOrDefaultAsync(x => x.AccountId == request.SenderAccountId && x.Client.Name == request.SenderClient);
            Validator.IsNull(senderAccount, ServiceConstants.InvalidInput);
            Validator.AccountCheck(senderAccount.AccountNumber, request.ReceiverAccount);

            var receiverAccount = await this.context.Accounts
                .Include(x => x.Client)
                .FirstOrDefaultAsync(x => x.AccountNumber == request.ReceiverAccount && x.Client.Name == request.ReceiverClient);
            Validator.IsNull(receiverAccount, ServiceConstants.InvalidInput);

            transaction.Description = request.Description;
            transaction.Amount = request.Amount;
            transaction.ReceiverAccountId = receiverAccount.AccountId;
            transaction.SenderAccountId = senderAccount.AccountId;

            this.context.Transactions.Update(transaction);
            await this.context.SaveChangesAsync();

            return transaction.ToDTO();
        }

        public async Task<TransactionDTO> SendSavedAsync(int id, TransactionRequest request)
        {
                Validator.TransactionRequest(request, ServiceConstants.InvalidInput);

                var transaction = await this.context.Transactions.FirstOrDefaultAsync(x => x.TransactionId == id);
                Validator.IsNull(transaction, ServiceConstants.InvalidInput);

                var senderAccount = await this.context.Accounts
                    .Include(x => x.Client)
                    .FirstOrDefaultAsync(x => x.AccountId == request.SenderAccountId && x.Client.Name == request.SenderClient);
                Validator.IsNull(senderAccount, ServiceConstants.InvalidInput);
                Validator.AccountCheck(senderAccount.AccountNumber, request.ReceiverAccount);
                Validator.SufficientAmount(senderAccount.Balance, request.Amount);

                var receiverAccount = await this.context.Accounts
                    .Include(x => x.Client)
                    .FirstOrDefaultAsync(x => x.AccountNumber == request.ReceiverAccount && x.Client.Name == request.ReceiverClient);
                Validator.IsNull(receiverAccount, ServiceConstants.InvalidInput);

                transaction.Description = request.Description;
                transaction.Amount = request.Amount;
                transaction.ReceiverAccountId = receiverAccount.AccountId;
                transaction.SenderAccountId = senderAccount.AccountId;
                transaction.StatusId = 1;
                transaction.MovementId = 2;

                var incomingTransaction = new Transaction()
                {
                    Description = transaction.Description,
                    Amount = transaction.Amount,
                    TimeStamp = transaction.TimeStamp,
                    StatusId = 1,
                    SenderAccountId = senderAccount.AccountId,
                    ReceiverAccountId = receiverAccount.AccountId,
                    MovementId = 1
                };

                senderAccount.Balance -= request.Amount;
                receiverAccount.Balance += request.Amount;

                this.context.Accounts.Update(senderAccount);
                this.context.Accounts.Update(receiverAccount);
                this.context.Transactions.Update(transaction);
                await this.context.Transactions.AddAsync(incomingTransaction);
                await this.context.SaveChangesAsync();

                return transaction.ToDTO();
        }

        public async Task<TransactionDTO> GetTransactionAsync(int id)
        {
            var transaction = await this.context.Transactions
                .Where(x => x.TransactionId == id)
                .Include(x => x.ReceiverAccount)
                    .ThenInclude(x => x.Client)
                .Include(x => x.SenderAccount)
                .Include(x => x.Status)
                .FirstOrDefaultAsync();

            Validator.IsNull(transaction, ServiceConstants.TransactionNotFound);

            return transaction.ToDTO();
        }

        public async Task<TransactionDTO> SendAsync(int id)
        {
            var transaction = await this.context.Transactions.FindAsync(id);
            Validator.IsNull(transaction, ServiceConstants.InvalidInput);

            var senderAccount = await this.context.Accounts
                    .FirstOrDefaultAsync(x => x.AccountId == transaction.SenderAccountId);
            Validator.SufficientAmount(senderAccount.Balance, transaction.Amount);

            var receiverAccount = await this.context.Accounts
                .FirstOrDefaultAsync(x => x.AccountId == transaction.ReceiverAccountId);

            transaction.StatusId = 1;
            transaction.MovementId = 2;

            var incomingTransaction = new Transaction()
            {
                Description = transaction.Description,
                Amount = transaction.Amount,
                TimeStamp = transaction.TimeStamp,
                StatusId = 1,
                SenderAccountId = transaction.SenderAccountId,
                ReceiverAccountId = transaction.ReceiverAccountId,
                MovementId = 1
            };

            senderAccount.Balance -= transaction.Amount;
            receiverAccount.Balance += transaction.Amount;

            this.context.Accounts.Update(senderAccount);
            this.context.Accounts.Update(receiverAccount);
            this.context.Transactions.Update(transaction);
            await this.context.Transactions.AddAsync(incomingTransaction);
            await this.context.SaveChangesAsync();

            return transaction.ToDTO();
        }

        public async Task<int> SavedTransactionsCountAsync(FilteredTransactionsRequest request)
        {
            var accounts = await LoggedUserAccountsAsync(request.UserId);
            var transactions = FilterSavedTransactions(accounts, request.SenderAccount, request.ReceiverAccount, request.UserAccount);

            return transactions.Count();
        }

        public async Task<ICollection<TransactionDTO>> GetSavedTransactionsAsync(int skip, int pageSize, FilteredTransactionsRequest request)
        {
            var accounts = await LoggedUserAccountsAsync(request.UserId);
            var transactions = FilterSavedTransactions(accounts, request.SenderAccount, request.ReceiverAccount, request.UserAccount);

            return await transactions
                .OrderByDescending(x => x.TimeStamp)
                .Skip(skip)
                .Take(pageSize)
                .Include(x => x.ReceiverAccount)
                    .ThenInclude(x => x.Client)
                .Include(x => x.SenderAccount)
                    .ThenInclude(x => x.Client)
                .Include(x => x.Status)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        public async Task<int> SentTransactionsCountAsync(FilteredTransactionsRequest request)
        {
            var accounts = await LoggedUserAccountsAsync(request.UserId);
            var transactions = FilterSentTransactions(accounts, request.SenderAccount, request.ReceiverAccount, request.UserAccount);

            return transactions.Count();
        }

        public async Task<ICollection<TransactionDTO>> GetTransactionsAsync(int skip, int pageSize, FilteredTransactionsRequest request)
        {
            var accounts = await LoggedUserAccountsAsync(request.UserId);
            var transactions = FilterSentTransactions(accounts, request.SenderAccount, request.ReceiverAccount, request.UserAccount);

            return await transactions
                .OrderByDescending(x => x.TimeStamp)
                .Skip(skip)
                .Take(pageSize)
                .Include(x=>x.Status)
                .Include(x=>x.Movement)
                .Include(x => x.SenderAccount)
                    .ThenInclude(x => x.Client)
                .Include(x => x.ReceiverAccount)
                    .ThenInclude(x => x.Client)
                .Select(x=>x.ToDTO())
                .ToListAsync();
        }

        public async Task<ICollection<AccountDTO>> SearchAsync(string text)
        {
            return await this.context.Accounts
                .Include(x => x.Client)
                .Where(x => x.AccountNumber.Contains(text))
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        public async Task<ICollection<AccountDTO>> SenderAccountNumbersAsync(int userId)
        {
            var accounts = await LoggedUserAccountsAsync(userId);

            var transactions = await this.context.Transactions
                .Where(x => accounts.Contains(x.SenderAccountId) || accounts.Contains(x.ReceiverAccountId))
                .Include(x => x.SenderAccount)
                .GroupBy(x => x.SenderAccountId)
                .Select(x => x.FirstOrDefault())
                .ToListAsync();

            var senderAccounts = new List<AccountDTO>();
            foreach (var transaction in transactions)
            {
                senderAccounts.Add(new AccountDTO()
                {
                    AccountId = transaction.SenderAccountId,
                    AccountNumber = transaction.SenderAccount.AccountNumber,
                    Balance = transaction.SenderAccount.Balance
                });
            }

            return senderAccounts
                .OrderBy(x => x.AccountNumber)
                .ToList();
        }

        public async Task<ICollection<AccountDTO>> ReceiverAccountNumbersAsync(int userId)
        {
            var accounts = await LoggedUserAccountsAsync(userId);

            var transactions = await this.context.Transactions
                .Where(x => accounts.Contains(x.ReceiverAccountId) || accounts.Contains(x.SenderAccountId))
                .Include(x => x.ReceiverAccount)
                .GroupBy(x => x.ReceiverAccountId)
                .Select(x => x.FirstOrDefault())
                .ToListAsync();

            var receiverAccounts = new List<AccountDTO>();
            foreach (var transaction in transactions)
            {
                receiverAccounts.Add(new AccountDTO()
                {
                    AccountId = transaction.ReceiverAccountId,
                    AccountNumber = transaction.ReceiverAccount.AccountNumber,
                    Balance = transaction.ReceiverAccount.Balance
                });
            }

            return receiverAccounts
                .OrderBy(x => x.AccountNumber)
                .ToList();
        }

        private async Task<ICollection<int>> LoggedUserAccountsAsync(int userId)
        {
            return await this.context.UsersAccounts
               .Where(u => u.UserId == userId)
               .Select(x => x.AccountId)
               .ToListAsync();
        }

        private IQueryable<Transaction> FilterSentTransactions(ICollection<int> accounts, 
            PayAccountRequest firstAccount, PayAccountRequest secondAccount, PayAccountRequest userAccount)
        {
            var transactions = this.context.Transactions
                 .Where(x => (accounts.Contains(x.ReceiverAccountId) && x.MovementId == 1 && x.StatusId == 1)
                 || (accounts.Contains(x.SenderAccountId) && x.MovementId == 2 && x.StatusId == 1));

            if (firstAccount.AccountNumber != null)
            {
                transactions = transactions.Where(x => x.SenderAccountId == firstAccount.AccountId);
            }

            if (secondAccount.AccountNumber != null)
            {
                transactions = transactions.Where(x => x.ReceiverAccountId == secondAccount.AccountId);
            }

            if (userAccount.AccountNumber != null)
            {
                transactions = transactions.Where(x => (x.ReceiverAccount.AccountNumber == userAccount.AccountNumber && x.MovementId == 1)
                                              || (x.SenderAccount.AccountNumber == userAccount.AccountNumber && x.MovementId == 2));
            }

            return transactions;
        }

        private IQueryable<Transaction> FilterSavedTransactions(ICollection<int> accounts,
            PayAccountRequest firstAccount, PayAccountRequest secondAccount, PayAccountRequest userAccount)
        {
            var transactions = this.context.Transactions
                .Where(x => accounts.Contains(x.SenderAccountId) && x.StatusId == 2);

            if (firstAccount.AccountNumber != null)
            {
                transactions = transactions.Where(x => x.SenderAccountId == firstAccount.AccountId);
            }

            if (secondAccount.AccountNumber != null)
            {
                transactions = transactions.Where(x => x.ReceiverAccountId == secondAccount.AccountId);
            }

            if (userAccount.AccountNumber != null)
            {
                transactions = transactions.Where(x => x.SenderAccountId == userAccount.AccountId);
            }

            return transactions;
        }
    }
}
