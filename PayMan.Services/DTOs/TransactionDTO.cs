﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.DTOs
{
    public class TransactionDTO

    {
        public int TransactionId { get; set; }

        public string Description { get; set; }

        public decimal Amount { get; set; }

        public DateTime TimeStamp { get; set; }

        public string SenderAccount { get; set; }

        public string ReceiverAccount { get; set; }

        public string ReceiverClient { get; set; }

        public string SenderClient { get; set; }

        public string Status { get; set; }

        public string Movement { get; set; }
    }
}
