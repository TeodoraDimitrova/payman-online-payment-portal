﻿using PayMan.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.DTOs
{
    public class ClientDTO
    {
        public int ClientId { get; set; }

        public string Name { get; set; }

        public ICollection<Account> Accounts { get; set; }

        public ICollection<UsersClients> UsersClients { get; set; }
    }
}
