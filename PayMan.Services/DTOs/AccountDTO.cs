﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.DTOs
{
    public class AccountDTO
    {
        public int AccountId { get; set; }

        public string AccountNumber { get; set; }

        public decimal Balance { get; set; }

        public string ClientName { get; set; }
    }
}
