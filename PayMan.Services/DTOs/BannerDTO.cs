﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.DTOs
{
    public class BannerDTO
    {
        public int BannerId { get; set; }

        public string ImageName { get; set; }

        public string URL { get; set; }

        public DateTime StartValidity { get; set; }

        public DateTime EndValidity { get; set; }
    }
}
