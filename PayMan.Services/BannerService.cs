﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Services.Mappers;
using PayMan.Services.Requests;
using PayMan.Services.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services
{
    public class BannerService : IBannerService
    {
        private readonly PayContext context;

        public BannerService(PayContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<BannerDTO> AddBannerAsync(string root, BannerRequest request)
        {
            Validator.IsNull(root, ServiceConstants.InvalidInput);
            Validator.IsNull(request?.Image, ServiceConstants.InvalidInput);
            Validator.Request(request, ServiceConstants.InvalidInput);

            var imageName = Guid.NewGuid() + ".jpg";
            var path = Path.Combine(root, imageName);

            using (var fileStream = File.Create(path))
            {
                await request.Image.CopyToAsync(fileStream);
            }

            var banner = new Banner()
            {
                ImageName = imageName,
                URL = request.URL,
                StartValidity = request.StartValidity,
                EndValidity = request.EndValidity
            };

            await this.context.Banners.AddAsync(banner);
            await this.context.SaveChangesAsync();

            return banner.ToDTO();
        }

        public async Task<BannerDTO> UpdateBannerAsync(int id, string root, BannerRequest request)
        {
            Validator.IsNull(root, ServiceConstants.InvalidInput);
            Validator.Request(request, ServiceConstants.InvalidInput);

            var banner = await this.context.Banners.FindAsync(id);
            Validator.IsNull(banner, ServiceConstants.InvalidInput);

            if (request.Image != null)
            {
                var path = Path.Combine(root, banner.ImageName);

                using (var fileStream = File.Create(path))
                {
                    await request.Image.CopyToAsync(fileStream);
                }
            }

            banner.URL = request.URL;
            banner.StartValidity = request.StartValidity;
            banner.EndValidity = request.EndValidity;

            this.context.Banners.Update(banner);
            await this.context.SaveChangesAsync();

            return banner.ToDTO();
        }

        public async Task DeleteBannerAsync(int id, string root)
        {
            var banner = await this.context.Banners.FindAsync(id);
            Validator.IsNull(banner, ServiceConstants.InvalidInput);

            var path = Path.Combine(root, banner.ImageName);
            File.Delete(path);

            this.context.Banners.Remove(banner);
            await this.context.SaveChangesAsync();
        }

        public async Task<BannerDTO> RandomBannerAsync()
        {
            var currentDate = DateTime.UtcNow;

            var banners = await this.context.Banners
                .Where(x => x.StartValidity.ToUniversalTime() <= currentDate &&
                    x.EndValidity.ToUniversalTime() > currentDate)
                .ToListAsync();

            if (banners.Count == 0)
            {
                return null;
            }

            Random random = new Random();
            int bannerNumber = random.Next(0, banners.Count);

            return banners[bannerNumber].ToDTO();
        }

        public async Task<BannerDTO> GetBannerAsync(int id)
        {
            var banner = await this.context.Banners.FindAsync(id);
            Validator.IsNull(banner, ServiceConstants.InvalidInput);

            return banner.ToDTO();
        }

        public async Task<ICollection<BannerDTO>> GetAllAsync()
        {
            return await this.context.Banners
                .Select(x => x.ToDTO())
                .ToListAsync();
        }
    }
}
