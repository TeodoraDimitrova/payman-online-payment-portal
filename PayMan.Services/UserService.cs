﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using PayMan.Data.Utilities;
using PayMan.Services.Contracts;
using PayMan.Services.Exceptions;
using PayMan.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Services
{
    public class UserService : IUserService
    {
        private readonly PayContext context;

        public UserService(PayContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<UserApp> CreateAsync(string userName, string name, string password)
        {
            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                throw new UserException("Password is required");
            }
            var user = new UserApp
            {
                UserName = userName,
                FullName = name,
                RoleId = 2,
            };

            byte[] passwordHash, passwordSalt;
            HashPassword.Generate(password, out passwordHash, out passwordSalt);

            var passwordSaltAsString = Convert.ToBase64String(passwordSalt);
            var passwordHashAsString = Convert.ToBase64String(passwordHash);

            user.PasswordHash = passwordHashAsString;
            user.PasswordSalt = passwordSaltAsString;

            await this.context.Users.AddAsync(user);
            await this.context.SaveChangesAsync();

            return user;
        }

        public async Task<List<string>> GetUsersNameAsync(string term)
        {
            var userNames = await this.context.Users
                            .Where(p => p.UserName.StartsWith(term))
                            .Select(p => p.UserName)
                            .ToListAsync();

            return userNames;
        }

        public async Task<ICollection<UserApp>> GetUsersAsync(int skip, int pageSize, string searchValue)
        {

            if (!string.IsNullOrEmpty(searchValue))
            {
                searchValue = searchValue.ToLower();

                return await this.context.Users
                    .Where(a => a.UserName.ToLower()
                    .StartsWith(searchValue))
                    .OrderBy(a => a.UserName)
                    .Skip(skip)
                    .Take(pageSize)
                    .ToListAsync();
            }
            else
            {
                var clients = await this.context.Users
                    .OrderBy(a => a.UserName)
                    .Skip(skip)
                    .Take(pageSize)
                    .ToListAsync();

                return clients;
            }
        }

        public async Task<int> GetTotalUsersCountAsync()
        {
            return await this.context.Users.CountAsync();
        }

        public async Task<bool> ExistUserAsync(string username)
        {
            var isUserExist = await this.context.Users.AnyAsync(x => x.UserName == username);
            return isUserExist;
        }

        //User Log In
        public async Task<UserApp> AuthenticateAsync(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                throw new UserException("Input data should be correct");
            }
            var user = await context.Users
                         .Include(r => r.Role)
                         .FirstOrDefaultAsync(u => u.UserName == username);

            Validator.IsNull(user, ServiceConstants.CanNotBeNull);


            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                throw new UserException("Password is not correct");
            }

            // authentication successful
            return user;
        }

        public ClaimsPrincipal CreateClaimsPrincipal(UserApp user, string scheme)
        {
            return new ClaimsPrincipal(CreateClaimsIdentity(user, scheme));
        }

        private ClaimsIdentity CreateClaimsIdentity(UserApp user, string scheme)
        {
            return new ClaimsIdentity(PrepareClaims(user), scheme);
        }

        private List<Claim> PrepareClaims(UserApp user)
        {
            return new List<Claim>
            {
              new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
              new Claim(ClaimTypes.Name, user.UserName),
              new Claim(ClaimTypes.Role, user.Role.Name)
            };
        }

        public AuthenticationProperties GetCookieOptions()
        {
            return new AuthenticationProperties
            {
                IsPersistent = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(30)
            };
        }

        //Admin Log In
        public async Task<Admin> AuthenticateAdminAsync(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                throw new UserException("Input data should be correct");
            }

            var admin = await this.context.Admins
                         .Include(r => r.Role)
                         .FirstOrDefaultAsync(u => u.UserName == username);

            // check if username exists
            Validator.IsNull(admin, ServiceConstants.CanNotBeNull);


            // check if password is correct
            if (!VerifyPasswordHash(password, admin.PasswordHash, admin.PasswordSalt))
            {
                throw new UserException("Password is not correct");
            }

            // authentication successful
            return admin;
        }

        public ClaimsPrincipal AdminClaimsPrincipal(Admin admin, string scheme)
        {
            return new ClaimsPrincipal(AdminClaimsIdentity(admin, scheme));
        }

        private ClaimsIdentity AdminClaimsIdentity(Admin admin, string scheme)
        {
            return new ClaimsIdentity(PrepareAdminClaims(admin), scheme);
        }

        private List<Claim> PrepareAdminClaims(Admin admin)
        {
            return new List<Claim>
            {
              new Claim(ClaimTypes.NameIdentifier, admin.AdminId.ToString()),
              new Claim(ClaimTypes.Name, admin.UserName),
              new Claim(ClaimTypes.Role, admin.Role.Name)
            };
        }


        private static bool VerifyPasswordHash(string password, string storedHash, string storedSalt)
        {
            var salt = Convert.FromBase64String(storedSalt);

            using (var hmac = new HMACSHA512(salt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                var hash = Convert.ToBase64String(computedHash);
                if (!hash.Equals(storedHash))
                {
                    return false;
                }
            }
            return true;
        }

    }
}
