﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.Requests
{
    public class FilteredTransactionsRequest
    {
        public int UserId { get; set; }

        public PayAccountRequest SenderAccount { get; set; }

        public PayAccountRequest ReceiverAccount { get; set; }

        public PayAccountRequest UserAccount { get; set; }
    }
}
