﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.Requests
{
    public class BannerRequest
    {
        public string ImageName { get; set; }

        public IFormFile Image { get; set; }

        public string URL { get; set; }

        public DateTime StartValidity { get; set; }

        public DateTime EndValidity { get; set; }
    }
}
