﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.Requests
{
    public class PayAccountRequest
    {
        public int AccountId { get; set; }

        public string AccountNumber { get; set; }
    }
}
