﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Services.Requests
{
    public class TransactionRequest
    {
        public string Description { get; set; }

        public decimal Amount { get; set; }

        public string SenderClient { get; set; }

        public int SenderAccountId { get; set; }
               
        public string ReceiverClient { get; set; }
               
        public string ReceiverAccount { get; set; }
    }
}
