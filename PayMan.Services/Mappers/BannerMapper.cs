﻿using PayMan.Data.Models;
using PayMan.Services.DTOs;

namespace PayMan.Services.Mappers
{
    public static class BannerMapper
    {
        public static BannerDTO ToDTO(this Banner entity)
        {
            if (entity == null)
            {
                return null;
            }

            var dto = new BannerDTO()
            {
                BannerId = entity.BannerId,
                ImageName = entity.ImageName,
                URL = entity.URL,
                StartValidity = entity.StartValidity,
                EndValidity = entity.EndValidity
            };

            return dto;
        }
    }
}
