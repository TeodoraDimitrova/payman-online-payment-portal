﻿using PayMan.Data.Models;
using PayMan.Services.DTOs;

namespace PayMan.Services.Mappers
{
    public static class ClientMapper
    {
        public static ClientDTO ToDTO(this Client entity)
        {
            if (entity == null)
            {
                return null;
            }

            var dto = new ClientDTO()
            {
                 ClientId = entity.ClientId,
                 Name = entity.Name,
                 Accounts = entity.Accounts,
                 UsersClients =entity.UsersClients,
            };

            return dto;
        }
    }
}
