﻿using PayMan.Data.Models;
using PayMan.Data.Utilities;
using PayMan.Services.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace PayMan.Services.Mappers
{
    public static class TransactionMapper
    {
        public static TransactionDTO ToDTO(this Transaction entity)
        {
            if (entity == null)
            {
                return null;
            }

            var dto = new TransactionDTO()
            {
                TransactionId = entity.TransactionId,
                ReceiverAccount = entity.ReceiverAccount?.AccountNumber,
                ReceiverClient = entity.ReceiverAccount?.Client?.Name,
                SenderAccount = entity.SenderAccount?.AccountNumber,
                SenderClient = entity.SenderAccount?.Client?.Name,
                Description = entity.Description,
                Amount = entity.Amount,
                TimeStamp = entity.TimeStamp,
                Status = entity.Status?.Name,
                Movement = entity.Movement?.Name
            };

            return dto;
        }

        public static IEnumerable<TransactionDTO> ToDTO(this IEnumerable<Transaction> transaction)
        {
            return transaction.Select(x => x.ToDTO());
        }
    }
}
