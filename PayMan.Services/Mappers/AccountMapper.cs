﻿using PayMan.Data.Models;
using PayMan.Services.DTOs;

namespace PayMan.Services.Mappers
{
    public static class AccountMapper
    {
        public static AccountDTO ToDTO(this Account entity)
        {
            if (entity == null)
            {
                return null;
            }

            var dto = new AccountDTO()
            {
                AccountId = entity.AccountId,
                AccountNumber = entity.AccountNumber,
                ClientName = entity.Client.Name,
                Balance = entity.Balance
            };

            return dto;
        }
    }
}
