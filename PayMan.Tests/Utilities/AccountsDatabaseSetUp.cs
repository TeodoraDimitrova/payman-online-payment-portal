﻿using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Utilities
{
    class AccountsDatabaseSetUp
    {
        public static async Task ArrangeContextAsync(DbContextOptions options)
        {
            using (var arrangeContext = new PayContext(options))
            {
                var client = new Client()
                {
                    ClientId = 1,
                    Name = "Happy",
                    Accounts=new List<Account>(),
                    UsersClients=new List<UsersClients>()
                };
                var client2 = new Client()
                {
                    ClientId = 2,
                    Name = "Peppy",
                    Accounts = new List<Account>(),
                    UsersClients = new List<UsersClients>()
                };

                var user = new UserApp()
                {
                    UserId = 1,
                    FullName = "Ivan Ivanov",
                    UserName = "Vankata",
                    PasswordHash = "Parola",
                    RoleId = 2                      
                };

                var account = new Account()
                {
                    AccountId = 3,
                    AccountNumber = "1234567890",
                    ClientId = 1,
                    Balance = 1000,
                };
                var account2 = new Account()
                {
                    AccountId = 4,
                    AccountNumber = "9876543210",
                    ClientId = 1,
                    Balance = 1000,
                };

                var usersClients = new UsersClients()
                {
                    UserId = 1,
                    ClientId = 1
                };

                var usersAccounts = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 1,
                    Nickname = "1234567890"
                };

                var usersAccounts2 = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 3,
                    Nickname = "more"
                };

                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.Clients.AddAsync(client2);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Accounts.AddAsync(account);
                await arrangeContext.Accounts.AddAsync(account2);
                await arrangeContext.UsersClients.AddAsync(usersClients);
                await arrangeContext.UsersAccounts.AddAsync(usersAccounts);
                await arrangeContext.UsersAccounts.AddAsync(usersAccounts2);
                await arrangeContext.SaveChangesAsync();
            }
        }        
    }
}