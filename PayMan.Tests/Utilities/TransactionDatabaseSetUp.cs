﻿using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Utilities
{
    public static class TransactionDatabaseSetUp
    {
        public static async Task ArrangeContextAsync(DbContextOptions options)
        {
            using (var arrangeContext = new PayContext(options))
            {
                var client = new Client()
                {
                    ClientId = 1,
                    Name = "Happy",
                };

                var role = new Role()
                {
                    RoleId = 2,
                    Name = "User"
                };

                var movement = new Movement()
                {
                    MovementId = 1,
                    Name = "Incoming"
                };

                var statusT = new Status()
                {
                    StatusId = 1,
                    Name = "Sent"
                };

                var statusF = new Status()
                {
                    StatusId = 2,
                    Name = "Saved"
                };

                var user = new UserApp()
                {
                    UserId = 1,
                    FullName = "Ivan Ivanov",
                    UserName = "Vankata",
                    PasswordHash = "Parola",
                    RoleId = 2
                };
                
                var account = new Account()
                {
                    AccountId = 1,
                    AccountNumber = "1234567890",
                    ClientId = 1,
                    Balance = 1000,
                };

                var account1 = new Account()
                {
                    AccountId = 2,
                    AccountNumber = "9876543210",
                    ClientId = 1,
                    Balance = 1000,
                };

                var usersAccounts1 = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 1
                };

                var usersAccounts2 = new UsersAccounts()
                {
                    UserId = 1,
                    AccountId = 2
                };

                var transaction1 = new Transaction()
                {
                    TransactionId = 1,
                    ReceiverAccountId = 1,
                    SenderAccountId = 2,
                    Description = "Rent",
                    Amount = 100,
                    StatusId = 2,
                    MovementId = 1,
                    TimeStamp = new DateTime(2019, 5, 5)
                };

                var transaction2 = new Transaction()
                {
                    TransactionId = 2,
                    ReceiverAccountId = 2,
                    SenderAccountId = 1,
                    Description = "Extra charges",
                    Amount = 200,
                    StatusId = 1,
                    MovementId = 1,
                    TimeStamp = new DateTime(2019, 6, 6)
                };
                var transaction3 = new Transaction()
                {
                    TransactionId = 3,
                    ReceiverAccountId = 2,
                    SenderAccountId = 1,
                    Description = "Extra charges",
                    Amount = 1200,
                    StatusId = 1,
                    TimeStamp = new DateTime(2019, 6, 6)
                };

                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.Roles.AddAsync(role);
                await arrangeContext.Movements.AddAsync(movement);
                await arrangeContext.Statuses.AddAsync(statusT);
                await arrangeContext.Statuses.AddAsync(statusF);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Accounts.AddAsync(account);
                await arrangeContext.Accounts.AddAsync(account1);
                await arrangeContext.UsersAccounts.AddAsync(usersAccounts1);
                await arrangeContext.UsersAccounts.AddAsync(usersAccounts2);
                await arrangeContext.Transactions.AddAsync(transaction1);
                await arrangeContext.Transactions.AddAsync(transaction2);
                await arrangeContext.Transactions.AddAsync(transaction3);
                await arrangeContext.SaveChangesAsync();
            }
        }

        public static async Task NewTransactionContextAsync(DbContextOptions options)
        {
            using (var arrangeContext = new PayContext(options))
            {
                var client = new Client()
                {
                    ClientId = 1,
                    Name = "Happy",
                };

                var statusT = new Status()
                {
                    StatusId = 1,
                    Name = "Sent"
                };

                var statusF = new Status()
                {
                    StatusId = 2,
                    Name = "Saved"
                };

                var movement = new Movement()
                {
                    MovementId = 2,
                    Name = "Outgoing"
                };

                var account = new Account()
                {
                    AccountId = 1,
                    AccountNumber = "1234567890",
                    ClientId = 1,
                    Balance = 1000,
                };

                var account1 = new Account()
                {
                    AccountId = 2,
                    AccountNumber = "9876543210",
                    ClientId = 1,
                    Balance = 1000,
                };

                var transaction2 = new Transaction()
                {
                    TransactionId = 3,
                    ReceiverAccountId = 1,
                    SenderAccountId = 2,
                    Description = "Rent",
                    Amount = 100,
                    StatusId = 1,
                    TimeStamp = new DateTime(2019, 5, 5)
                };

                var transaction3 = new Transaction()
                {
                    TransactionId = 4,
                    ReceiverAccountId = 2,
                    SenderAccountId = 1,
                    Description = "Extra charges",
                    Amount = 200,
                    StatusId = 1,
                    TimeStamp = new DateTime(2019, 6, 6)
                };

                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.Statuses.AddAsync(statusT);
                await arrangeContext.Statuses.AddAsync(statusF);
                await arrangeContext.Movements.AddAsync(movement);
                await arrangeContext.Accounts.AddAsync(account);
                await arrangeContext.Accounts.AddAsync(account1);
                await arrangeContext.Transactions.AddAsync(transaction2);
                await arrangeContext.Transactions.AddAsync(transaction3);
                await arrangeContext.SaveChangesAsync();
            }
        }
    }
}
