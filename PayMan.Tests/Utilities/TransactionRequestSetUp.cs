﻿using PayMan.Services.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace PayMan.Tests.Utilities
{
    public static class TransactionRequestSetUp
    {
        public static TransactionRequest Request()
        {
            var request = new TransactionRequest()
            {
                ReceiverClient = "Happy",
                ReceiverAccount = "1234567890",
                SenderClient = "Happy",
                SenderAccountId = 2,
                Description = "Purchase",
                Amount = 300
            };
            return request;
        }

        public static TransactionRequest ExceptionRequest()
        {
            var request = new TransactionRequest()
            {
                ReceiverClient = "Happy",
                ReceiverAccount = "1234567890",
                SenderClient = "Happy",
                SenderAccountId = 2,
                Description = "Purchase",
                Amount = 300000
            };
            return request;
        }

        public static TransactionRequest ExceptionNumberRequest()
        {
            var request = new TransactionRequest()
            {
                ReceiverClient = "Happy",
                ReceiverAccount = "1234567891",
                SenderClient = "Happy",
                SenderAccountId = 2,
                Description = "Purchase",
                Amount = 300
            };
            return request;
        }

        public static TransactionRequest AccountNumbersException()
        {
            var request = new TransactionRequest()
            {
                ReceiverClient = "Happy",
                ReceiverAccount = "1234567891",
                SenderClient = "Happy",
                SenderAccountId = 1,
                Description = "Purchase",
                Amount = 300
            };
            return request;
        }

        public static FilteredTransactionsRequest FilterRequest()
        {
            var request = new FilteredTransactionsRequest()
            {
                UserId = 1,
                SenderAccount = new PayAccountRequest() { AccountNumber = null },
                ReceiverAccount = new PayAccountRequest() { AccountNumber = null },
                UserAccount = new PayAccountRequest() { AccountNumber = null }
            };

            return request;
        }
    }
}
