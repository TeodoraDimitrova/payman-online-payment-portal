﻿using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Utilities
{
    class ClientDatabaseSetUp
    {
        public static async Task ArrangeContextAsync(DbContextOptions options)
        {
            using (var arrangeContext = new PayContext(options))
            {
                var account = new Account()
                {
                    AccountId = 1,
                    AccountNumber = "1234567890",
                    Balance = 1000,
                    ClientId = 1
                };

                var accounts = new List<Account>();
                accounts.Add(account);


                var client = new Client()
                {
                    ClientId = 1,
                    Name = "Test",

                };

                var client2 = new Client()
                {
                    ClientId = 2,
                    Name = "Test2",
                    Accounts = accounts
                };

                var user = new UserApp()
                {
                    UserId = 1,
                    UserName = "didi89",

                };

                var user2 = new UserApp()
                {
                    UserId = 2,
                    UserName = "bety91",

                };

                var usersClients = new UsersClients()
                {
                    ClientId = 1,
                    UserId = 1
                };

                var usersClients2 = new UsersClients()
                {
                    ClientId = 2,
                    UserId = 1
                };

                await arrangeContext.Clients.AddAsync(client);
                await arrangeContext.Clients.AddAsync(client2);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Users.AddAsync(user2);
                await arrangeContext.UsersClients.AddAsync(usersClients);
                await arrangeContext.SaveChangesAsync();
            }
        }
    }
}
