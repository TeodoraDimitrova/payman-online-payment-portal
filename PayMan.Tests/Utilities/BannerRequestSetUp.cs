﻿using Microsoft.AspNetCore.Http;
using Moq;
using PayMan.Services.Requests;
using System;

namespace PayMan.Tests.Utilities
{
    public static class BannerRequestSetUp
    {
        public static BannerRequest Request()
        {
            var request = new BannerRequest()
            {
                ImageName = "1.jpg",
                URL = "www.google.com",
                Image = new Mock<IFormFile>().Object,
                StartValidity = new DateTime(2019, 05, 05),
                EndValidity = new DateTime(2019, 06, 05)
            };

            return request;
        }
    }
}
