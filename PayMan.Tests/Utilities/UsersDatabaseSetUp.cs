﻿using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Utilities
{
    class UsersDatabaseSetUp
    {
        public static async Task ArrangeContextAsync(DbContextOptions options)
        {
            using (var arrangeContext = new PayContext(options))
            {
                var admin = new Admin
                {
                    AdminId = 1,
                    UserName = "admin",
                    RoleId = 1,
                    PasswordHash="12345678",
                    PasswordSalt="12345678"                    
                };

                var user = new UserApp()
                {
                    UserId = 1,
                    RoleId = 1,
                    UserName = "didi89",
                    FullName = "Diana",
                    PasswordHash = "12345678",
                    PasswordSalt = "12345678"
                };

                var user2 = new UserApp()
                {
                    UserId = 2,
                    RoleId = 1,
                    UserName = "test",
                    FullName = "Test",
                    PasswordHash = "12345678",
                    PasswordSalt = "12345678"
                };

                var role = new Role()
                {
                    RoleId = 1,
                    Name = "User"
                };

                await arrangeContext.Admins.AddAsync(admin);
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.Users.AddAsync(user2);
                await arrangeContext.Roles.AddAsync(role);
                await arrangeContext.SaveChangesAsync();
            }
        }
    }
}
