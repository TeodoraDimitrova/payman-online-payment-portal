﻿using Microsoft.EntityFrameworkCore;
using PayMan.Data;
using PayMan.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Utilities
{
    public static class BannerDatabaseSetUp
    {
        public static async Task ArrangeContextAsync(DbContextOptions options)
        {
            using (var arrangeContext = new PayContext(options))
            {
                var banner = new Banner()
                {
                    BannerId = 1,
                    ImageName = "2bcfe4c5-a918-42f8-a524-018cecc03c92.jpg",
                    URL = "www.youtube.com",
                    StartValidity = new DateTime(2019, 05, 15),
                    EndValidity = new DateTime(2019, 06, 15)
                };

                await arrangeContext.Banners.AddAsync(banner);
                await arrangeContext.SaveChangesAsync();
            }
        }

        public static async Task ArrangeRandomContextAsync(DbContextOptions options)
        {
            using (var arrangeContext = new PayContext(options))
            {
                var banner = new Banner()
                {
                    BannerId = 1,
                    ImageName = "2bcfe4c5-a918-42f8-a524-018cecc03c92.jpg",
                    URL = "www.youtube.com",
                    StartValidity = new DateTime(2019, 01, 01),
                    EndValidity = new DateTime(2119, 01, 01)
                };
                
                await arrangeContext.Banners.AddAsync(banner);
                await arrangeContext.SaveChangesAsync();
            }
        }
    }
}
