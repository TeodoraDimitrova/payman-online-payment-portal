﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.DTOs;
using PayMan.Tests.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class SearchAsyncShould
    {
        [TestMethod]
        public async Task ReturnCorrectType()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectType));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var accounts = await sut.SearchAsync("1");

                Assert.IsInstanceOfType(accounts, typeof(ICollection<AccountDTO>));
            }
        }

        [TestMethod]
        public async Task ReturnCorrectAmountWhenSearchIsConducted()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectAmountWhenSearchIsConducted));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var accounts = await sut.SearchAsync("1");

                Assert.IsTrue(accounts.Count() == 2);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectItemsWhenSearchIsConducted()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectItemsWhenSearchIsConducted));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var accounts = await sut.SearchAsync("123");
                var first = accounts.FirstOrDefault(x => x.AccountId == 1);
                
                Assert.IsTrue(first.AccountId == 1);
                Assert.IsTrue(first.AccountNumber == "1234567890");
                Assert.IsTrue(first.ClientName == "Happy");
                Assert.IsTrue(first.Balance == 1000);
            }
        }
    }
}
