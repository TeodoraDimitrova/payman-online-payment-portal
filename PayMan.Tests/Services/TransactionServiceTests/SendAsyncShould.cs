﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class SendAsyncShould
    {
        [TestMethod]
        public async Task ReturnCorrectResultWithProperData()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectResultWithProperData));

            await TransactionDatabaseSetUp.NewTransactionContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.SendAsync(3);

                Assert.IsTrue(transaction.TransactionId == 3);
                Assert.IsTrue(transaction.Description == "Rent");
                Assert.IsTrue(transaction.Amount == 100);
                Assert.IsTrue(transaction.TimeStamp == new DateTime(2019, 5, 5));
            }
        }

        [TestMethod]
        public async Task ReturnCorrectInformationForBallanceInAccountsAfterSending()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectInformationForBallanceInAccountsAfterSending));

            await TransactionDatabaseSetUp.NewTransactionContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.SendAsync(3);

                var first = await assertContext.Accounts.FindAsync(1);
                var second = await assertContext.Accounts.FindAsync(2);

                Assert.IsTrue(first.Balance == 1100);
                Assert.IsTrue(second.Balance == 900);
            }
        }

        [TestMethod]
        public async Task ThrowWhenTransactionIdIsNotAvailableInDatabase()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenTransactionIdIsNotAvailableInDatabase));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendAsync(1));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderAccountsBalanceIsNotHighEnough()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderAccountsBalanceIsNotHighEnough));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendAsync(3));
            }
        }
    }
}
