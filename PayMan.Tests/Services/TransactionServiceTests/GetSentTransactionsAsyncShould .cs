﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Tests.Utilities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class GetSentTransactionsAsyncShould
    {
        [TestMethod]
        public async Task GetCorrectCountOfAllSent()
        {
            var options = DbOptions.GetOptions(nameof(GetCorrectCountOfAllSent));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var result = await sut.GetTransactionsAsync(0, 5, TransactionRequestSetUp.FilterRequest());

                Assert.IsTrue(result.Count() == 1);
            }
        }

        [TestMethod]
        public async Task GetCorrectInformationForAllSent()
        {
            var options = DbOptions.GetOptions(nameof(GetCorrectInformationForAllSent));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transactions = await sut.GetTransactionsAsync(0, 5, TransactionRequestSetUp.FilterRequest());
                var first = transactions.FirstOrDefault(x => x.TransactionId == 2);

                Assert.IsTrue(first.TransactionId == 2);
                Assert.IsTrue(first.ReceiverAccount == "9876543210");
                Assert.IsTrue(first.SenderAccount == "1234567890");
                Assert.IsTrue(first.Description == "Extra charges");
                Assert.IsTrue(first.Amount == 200);
                Assert.IsTrue(first.TimeStamp == new DateTime(2019, 6, 6));
            }
        }
    }
}
