﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class SendNewTransAsyncShould
    {
        [TestMethod]
        public async Task ReturnCorrectlyOnSend()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectlyOnSend));

            await TransactionDatabaseSetUp.NewTransactionContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.SendNewTransAsync(TransactionRequestSetUp.Request());

                Assert.IsTrue(transaction.ReceiverAccount == "1234567890");
                Assert.IsTrue(transaction.ReceiverClient == "Happy");
                Assert.IsTrue(transaction.SenderAccount == "9876543210");
                Assert.IsTrue(transaction.SenderClient == "Happy");
                Assert.IsTrue(transaction.Description == "Purchase");
                Assert.IsTrue(transaction.Amount == 300);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectAmountsInEachAccount()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectAmountsInEachAccount));

            await TransactionDatabaseSetUp.NewTransactionContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.SendNewTransAsync(TransactionRequestSetUp.Request());

                var first = await assertContext.Accounts.FindAsync(1);
                var second = await assertContext.Accounts.FindAsync(2);

                Assert.IsTrue(first.Balance == 1300);
                Assert.IsTrue(second.Balance == 700);
            }
        }

        [TestMethod]
        public async Task ThrowWhenSendTransactionRequestIsNull()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSendTransactionRequestIsNull));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendNewTransAsync(null));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderOrReceiversAccountsAreNotInDatabaseWhenSending()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderOrReceiversAccountsAreNotInDatabaseWhenSending));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendNewTransAsync(TransactionRequestSetUp.Request()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSendersBalanceIsNotEnough()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSendersBalanceIsNotEnough));

            await TransactionDatabaseSetUp.NewTransactionContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendNewTransAsync(TransactionRequestSetUp.ExceptionRequest()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderAndReceiverAccountsAreSame()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderAndReceiverAccountsAreSame));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendNewTransAsync(TransactionRequestSetUp.AccountNumbersException()));
            }
        }
    }
}
