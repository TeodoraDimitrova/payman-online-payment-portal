﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class UpdateTransactionAsyncShould
    {
        [TestMethod]
        public async Task SendCorrectlyExistingTransaction()
        {
            var options = DbOptions.GetOptions(nameof(SendCorrectlyExistingTransaction));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.UpdateTransactionAsync(2, TransactionRequestSetUp.Request());

                Assert.IsTrue(transaction.TransactionId == 2);
                Assert.IsTrue(transaction.ReceiverAccount == "1234567890");
                Assert.IsTrue(transaction.ReceiverClient == "Happy");
                Assert.IsTrue(transaction.SenderAccount == "9876543210");
                Assert.IsTrue(transaction.ReceiverClient == "Happy");
                Assert.IsTrue(transaction.Description == "Purchase");
                Assert.IsTrue(transaction.Amount == 300);
                Assert.IsTrue(transaction.TimeStamp == new DateTime(2019, 6, 6));
            }
        }

        [TestMethod]
        public async Task ThrowWhenUpdateSavedTransactionRequestIsNull()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenUpdateSavedTransactionRequestIsNull));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.UpdateTransactionAsync(1, null));
            }
        }

        [TestMethod]
        public async Task ThrowWhenUpdateSavedTransactionIdIsNotInDatabase()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenUpdateSavedTransactionIdIsNotInDatabase));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.UpdateTransactionAsync(1, TransactionRequestSetUp.Request()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderOrReceiversAccountsAreNotInDatabaseWhenUpdatingSaved()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderOrReceiversAccountsAreNotInDatabaseWhenUpdatingSaved));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.UpdateTransactionAsync(2, TransactionRequestSetUp.ExceptionNumberRequest()));
            }
        }


        [TestMethod]
        public async Task ThrowWhenSenderAndReceiverAccountsAreSameWhenUpdating()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderAndReceiverAccountsAreSameWhenUpdating));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendNewTransAsync(TransactionRequestSetUp.AccountNumbersException()));
            }
        }
    }
}
