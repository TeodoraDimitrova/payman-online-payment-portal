﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class SaveNewTransAsyncShould
    {
        [TestMethod]
        public async Task SaveNewTransactionCorrectly()
        {
            var options = DbOptions.GetOptions(nameof(SaveNewTransactionCorrectly));

            await TransactionDatabaseSetUp.NewTransactionContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.SaveNewTransAsync(TransactionRequestSetUp.Request());

                Assert.IsTrue(transaction.TransactionId == 1);
                Assert.IsTrue(transaction.ReceiverAccount == "1234567890");
                Assert.IsTrue(transaction.ReceiverClient == "Happy");
                Assert.IsTrue(transaction.SenderAccount == "9876543210");
                Assert.IsTrue(transaction.SenderClient == "Happy");
                Assert.IsTrue(transaction.Description == "Purchase");
                Assert.IsTrue(transaction.Amount == 300);
            }
        }

        [TestMethod]
        public async Task ThrowWhenTransactionRequestIsNull()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenTransactionRequestIsNull));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SaveNewTransAsync(null));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderOrReceiversAccountsAreNotInDatabase()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderOrReceiversAccountsAreNotInDatabase));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SaveNewTransAsync(TransactionRequestSetUp.Request()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderAndReceiverAccountsAreSameWhenSaving()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderAndReceiverAccountsAreSameWhenSaving));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendNewTransAsync(TransactionRequestSetUp.AccountNumbersException()));
            }
        }
    }
}
