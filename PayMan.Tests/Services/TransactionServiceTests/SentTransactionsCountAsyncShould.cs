﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Tests.Utilities;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class SentTransactionsCountAsyncShould
    {
        [TestMethod]
        public async Task ReturnCorrectAmountAfterFilterSentByAccount()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectAmountAfterFilterSentByAccount));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transactionsCount = await sut.SentTransactionsCountAsync(TransactionRequestSetUp.FilterRequest());

                Assert.IsTrue(transactionsCount == 1);
            }
        }
    }
}
