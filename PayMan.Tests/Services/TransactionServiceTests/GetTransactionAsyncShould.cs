﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class GetTransactionAsyncShould
    {
        [TestMethod]
        public async Task ReturnCorrectTransaction()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectTransaction));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.GetTransactionAsync(1);

                Assert.IsTrue(transaction.TransactionId == 1);
                Assert.IsTrue(transaction.SenderAccount == "9876543210");
                Assert.IsTrue(transaction.ReceiverClient == "Happy");
                Assert.IsTrue(transaction.ReceiverAccount == "1234567890");
                Assert.IsTrue(transaction.ReceiverClient == "Happy");
                Assert.IsTrue(transaction.Description == "Rent");
                Assert.IsTrue(transaction.Amount == 100);
                Assert.IsTrue(transaction.Status == "Saved");
                Assert.IsTrue(transaction.TimeStamp == new DateTime(2019, 5, 5));
            }
        }

        [TestMethod]
        public async Task ThrowWhenTransactionNotAvailable()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenTransactionNotAvailable));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.GetTransactionAsync(1));
            }
        }
    }
}
