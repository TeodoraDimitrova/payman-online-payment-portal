﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Tests.Utilities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class GetSavedTransactionsAsyncShould
    {
        [TestMethod]
        public async Task GetCorrectCountOfAllSaved()
        {
            var options = DbOptions.GetOptions(nameof(GetCorrectCountOfAllSaved));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var result = await sut.GetSavedTransactionsAsync(0, 5, TransactionRequestSetUp.FilterRequest());

                Assert.IsTrue(result.Count() == 1);
            }
        }

        [TestMethod]
        public async Task GetCorrectInformationForAllSaved()
        {
            var options = DbOptions.GetOptions(nameof(GetCorrectInformationForAllSaved));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transactions = await sut.GetSavedTransactionsAsync(0, 5, TransactionRequestSetUp.FilterRequest());
                var first = transactions.FirstOrDefault(x => x.TransactionId == 1);

                Assert.IsTrue(first.TransactionId == 1);
                Assert.IsTrue(first.ReceiverAccount == "1234567890");
                Assert.IsTrue(first.SenderAccount == "9876543210");
                Assert.IsTrue(first.Description == "Rent");
                Assert.IsTrue(first.Amount == 100);
                Assert.IsTrue(first.TimeStamp == new DateTime(2019, 5, 5));
            }
        }
    }
}
