﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class SendSavedAsyncShould
    {
        [TestMethod]
        public async Task ChangeCorrectlyExistingTransaction()
        {
            var options = DbOptions.GetOptions(nameof(ChangeCorrectlyExistingTransaction));

            await TransactionDatabaseSetUp.NewTransactionContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.SendSavedAsync(3, TransactionRequestSetUp.Request());

                Assert.IsTrue(transaction.TransactionId == 3);
                Assert.IsTrue(transaction.ReceiverAccount == "1234567890");
                Assert.IsTrue(transaction.ReceiverClient == "Happy");
                Assert.IsTrue(transaction.SenderAccount == "9876543210");
                Assert.IsTrue(transaction.ReceiverClient == "Happy");
                Assert.IsTrue(transaction.Description == "Purchase");
                Assert.IsTrue(transaction.Amount == 300);
                Assert.IsTrue(transaction.TimeStamp == new DateTime(2019, 5, 5));
            }
        }

        [TestMethod]
        public async Task ReturnCorrectAmountsInEachAccountAfterSending()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectAmountsInEachAccountAfterSending));

            await TransactionDatabaseSetUp.NewTransactionContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var transaction = await sut.SendSavedAsync(3, TransactionRequestSetUp.Request());

                var first = await assertContext.Accounts.FindAsync(1);
                var second = await assertContext.Accounts.FindAsync(2);

                Assert.IsTrue(first.Balance == 1300);
                Assert.IsTrue(second.Balance == 700);
            }
        }

        [TestMethod]
        public async Task ThrowWhenSendSavedTransactionRequestIsNull()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSendSavedTransactionRequestIsNull));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendSavedAsync(1, null));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSendSavedTransactionIdIsNotInDatabase()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSendSavedTransactionIdIsNotInDatabase));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendSavedAsync(1, TransactionRequestSetUp.Request()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderOrReceiversAccountsAreNotInDatabaseWhenSendingSaved()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderOrReceiversAccountsAreNotInDatabaseWhenSendingSaved));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendSavedAsync(2, TransactionRequestSetUp.ExceptionNumberRequest()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderAccountsBalanceIsNotEnoughWhenSendingSaved()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderOrReceiversAccountsAreNotInDatabaseWhenSendingSaved));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendSavedAsync(2, TransactionRequestSetUp.ExceptionRequest()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenSenderAndReceiverAccountsAreSameWhenSending()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenSenderAndReceiverAccountsAreSameWhenSending));

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.SendNewTransAsync(TransactionRequestSetUp.AccountNumbersException()));
            }
        }
    }
}
