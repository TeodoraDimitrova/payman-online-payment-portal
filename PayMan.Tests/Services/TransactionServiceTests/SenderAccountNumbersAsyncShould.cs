﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.DTOs;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.TransactionServiceTests
{
    [TestClass]
    public class SenderAccountNumbersAsyncShould
    {
        [TestMethod]
        public async Task ReturnCorrectTypeForSenderAccountNumbers()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectTypeForSenderAccountNumbers));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var accounts = await sut.SenderAccountNumbersAsync(1);

                Assert.IsInstanceOfType(accounts, typeof(ICollection<AccountDTO>));
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCountForSenderAccountNumbers()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectCountForSenderAccountNumbers));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var accounts = await sut.SenderAccountNumbersAsync(1);

                Assert.IsTrue(accounts.Count() == 2);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectAccountNumbers()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectAccountNumbers));

            await TransactionDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new TransactionService(assertContext);
                var accounts = await sut.SenderAccountNumbersAsync(1);
                var first = accounts.FirstOrDefault(x => x.AccountId == 1);
                var second = accounts.FirstOrDefault(x => x.AccountId == 2);

                Assert.IsTrue(first.AccountId == 1);
                Assert.IsTrue(second.AccountId == 2);
                Assert.IsTrue(first.AccountNumber == "1234567890");
                Assert.IsTrue(second.AccountNumber == "9876543210");
            }
        }
    }
}
