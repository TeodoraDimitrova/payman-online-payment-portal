﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.AccountServiceTests
{
    [TestClass]
    public class RemoveAccountFromUserAsyncShould_
    {
        [TestMethod]
        public async Task RemovesAccountFromUser()
        {
            var options = DbOptions.GetOptions(nameof(RemovesAccountFromUser));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                var userAccounts = await sut.RemoveAccountFromUserAsync(1,1);
               
                Assert.IsTrue(userAccounts.AccountId == 1);
                Assert.IsTrue(userAccounts.UserId == 1);
            }
        }
    }
}
