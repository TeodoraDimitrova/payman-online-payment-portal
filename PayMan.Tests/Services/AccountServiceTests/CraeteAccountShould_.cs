﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.AccountServiceTests
{
    [TestClass]
    public class CraeteAccountShould_
    {
        [TestMethod]
        public async Task ReturnCorrectAccountDTO()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectAccountDTO));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);
                var account = await sut.CreareAccountAsync(1000, 1);

                Assert.IsTrue(account.ClientName == "Happy");
                Assert.IsTrue(account.Balance==1000);
                Assert.IsTrue(account.AccountId == 1);
                Assert.IsFalse(account.AccountNumber.Contains('a'));
            }
        }

        [TestMethod]
        public async Task AddNewAccountToClient()
        {
            var options = DbOptions.GetOptions(nameof(AddNewAccountToClient));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);
                var account = await sut.CreareAccountAsync(1000,1);
                var account2 = await sut.CreareAccountAsync(2000, 2);

                Assert.IsTrue(account.ClientName=="Happy");
                Assert.IsTrue(account.Balance == 1000);
                Assert.IsTrue(account2.ClientName == "Peppy");
                Assert.IsTrue(account2.Balance == 2000);

            }
        }
        [TestMethod]
        public async Task Throw_WhenClientIsNull()
        {
            var options = DbOptions.GetOptions(nameof(Throw_WhenClientIsNull));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

               await Assert.ThrowsExceptionAsync<UserException>(()=> sut.CreareAccountAsync(1000, 3));
            }
        }
    }
}
