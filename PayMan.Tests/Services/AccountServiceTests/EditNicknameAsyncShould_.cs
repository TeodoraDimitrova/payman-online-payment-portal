﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.AccountServiceTests
{
    [TestClass]
    public class EditNicknameAsyncShould_
    {
        [TestMethod]
        public async Task EditNewNickname()
        {
            var options = DbOptions.GetOptions(nameof(EditNewNickname));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                var userAccounts = await sut.EditNickNameAsync(1,1,"lichni");

                Assert.IsTrue(userAccounts.Nickname== "lichni");
            }
        }   

        [TestMethod]
        public async Task Throw_WhenInputIsNotCorrect()
        {
            var options = DbOptions.GetOptions(nameof(Throw_WhenInputIsNotCorrect));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.EditNickNameAsync(5, 1, "lichni"));
            }
        }
    }
}
