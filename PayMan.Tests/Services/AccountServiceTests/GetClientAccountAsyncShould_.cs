﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.AccountServiceTests
{
    [TestClass]
    public class GetClientAccountAsyncShould_
    {
        [TestMethod]
        public async Task ReturnClientAccounts()
        {
            var options = DbOptions.GetOptions(nameof(ReturnClientAccounts));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                var clientAccounts = await sut.GetClientAccountsAsync(1);

                Assert.IsTrue(clientAccounts.Count == 2);
            }
        }

        [TestMethod]
        public async Task Throw_WhenClientIsNull()
        {
            var options = DbOptions.GetOptions(nameof(Throw_WhenClientIsNull));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(()=> sut.GetClientAccountsAsync(3));
            }
        }

        [TestMethod]
        public async Task ReturnAccountsDTO()
        {
            var options = DbOptions.GetOptions(nameof(ReturnAccountsDTO));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                var userAccounts = await sut.GetAccountsAsync(1);

                Assert.IsTrue(userAccounts.Count == 1);
            }
        }

        [TestMethod]
        public async Task ReturnUserAccounts()
        {
            var options = DbOptions.GetOptions(nameof(ReturnUserAccounts));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                var userAccounts = await sut.GetUserAccountsAsync(1);

                Assert.IsTrue(userAccounts.Count == 1);
            }
        }

        [TestMethod]
        public async Task ReturnAccountUsers()
        {
            var options = DbOptions.GetOptions(nameof(ReturnAccountUsers));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                var userAccounts = await sut.GetAccountUsersAsync(3);

                Assert.IsTrue(userAccounts.Count == 1);
            }
        }
    }
}
