﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.AccountServiceTests
{
    [TestClass]
    public class AddAccountToUserAsyncShould_
    {
        [TestMethod]
        public async Task AddAccountToUser()
        {
            var options = DbOptions.GetOptions(nameof(AddAccountToUser));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                var userAccounts = await sut.AddAccountToUserAsync(4,1);

                Assert.IsTrue(userAccounts.AccountId == 4);
                Assert.IsTrue(userAccounts.UserId == 1);
            }
        }

        [TestMethod]
        public async Task Throw_WhenUserIsNull()
        {
            var options = DbOptions.GetOptions(nameof(Throw_WhenUserIsNull));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AddAccountToUserAsync(3, 2));
            }
        }

        [TestMethod]
        public async Task Throw_WhenAccountIsNull()
        {
            var options = DbOptions.GetOptions(nameof(Throw_WhenAccountIsNull));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AddAccountToUserAsync(2, 1));
            }
        }

        [TestMethod]
        public async Task Throw_WhenUserIsNotFromClient()
        {
            var options = DbOptions.GetOptions(nameof(Throw_WhenAccountIsNull));

            await AccountsDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new AccountService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AddAccountToUserAsync(4, 2));
            }
        }
    }
}
