﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.UserServiceTests
{
    [TestClass]
    public class GetUserShould_
    {
        [TestMethod]
        public async Task ReturnCorrectUsers()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectUsers));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                var user = await sut.GetUsersAsync(0,5,"");

                Assert.IsTrue(user.Count()==2);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectSearchUsers()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectSearchUsers));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                var user = await sut.GetUsersAsync(0, 5, "di");

                Assert.IsTrue(user.Count() == 1);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectSearchUsersName()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectSearchUsersName));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                var user = await sut.GetUsersNameAsync("di");

                Assert.IsTrue(user.Count() == 1);
            }
        }

        [TestMethod]
        public async Task ReturnUsersCount()
        {
            var options = DbOptions.GetOptions(nameof(ReturnUsersCount));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                var count = await sut.GetTotalUsersCountAsync();

                Assert.IsTrue(count == 2);
            }
        }

        [TestMethod]
        public async Task ReturnTrueIfUserExist()
        {
            var options = DbOptions.GetOptions(nameof(ReturnTrueIfUserExist));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                var isExist = await sut.ExistUserAsync("didi89");

                Assert.IsTrue(isExist == true);
            }
        }
    }
}
