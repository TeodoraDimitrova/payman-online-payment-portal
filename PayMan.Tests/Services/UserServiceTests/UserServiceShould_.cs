﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.UserServiceTests
{
    [TestClass]
    public class UserServiceShould_
    {
        [TestMethod]
        public async Task CreateUser()
        {
            var options = DbOptions.GetOptions(nameof(CreateUser));

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                var user = await sut.CreateAsync("didi89","Diana","123456");

                Assert.IsTrue(user.UserName== "didi89");
                Assert.IsTrue(user.FullName=="Diana");
            }
        }

        [TestMethod]
        public async Task CreateUserShould_Thow_WhenInputIsNullOrEmpty()
        {
            var options = DbOptions.GetOptions(nameof(CreateUserShould_Thow_WhenInputIsNullOrEmpty));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.CreateAsync("","Diana" , "123456"));
            }
        }

        [TestMethod]
        public async Task AuthenticateUserShould_Thow_WhenUserNameIsEmpty()
        {
            var options = DbOptions.GetOptions(nameof(AuthenticateUserShould_Thow_WhenUserNameIsEmpty));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AuthenticateAsync("", "123456"));
            }
        }

        [TestMethod]
        public async Task AuthenticateUserShould_Thow_WhenPasswordIsEmpty()
        {
            var options = DbOptions.GetOptions(nameof(AuthenticateUserShould_Thow_WhenPasswordIsEmpty));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AuthenticateAsync("test", ""));
            }
        }

        [TestMethod]
        public async Task AuthenticateUserShould_Thow_WhenUserIsNull()
        {
            var options = DbOptions.GetOptions(nameof(AuthenticateUserShould_Thow_WhenUserIsNull));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AuthenticateAsync("asd", "123456"));
            }
        }

        [TestMethod]
        public async Task AuthenticateUserShould_Throw_WhenCantVerifyPassword()
        {
            var options = DbOptions.GetOptions(nameof(AuthenticateUserShould_Throw_WhenCantVerifyPassword));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AuthenticateAsync("didi89", "123456"));
            }
        }
        [TestMethod]
        public async Task AuthenticateAdminShould_Thow_WhenUserNameIsEmpty()
        {
            var options = DbOptions.GetOptions(nameof(AuthenticateAdminShould_Thow_WhenUserNameIsEmpty));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AuthenticateAdminAsync("", "123456"));
            }
        }

        [TestMethod]
        public async Task AuthenticateAdminShould_Thow_WhenPasswordIsEmpty()
        {
            var options = DbOptions.GetOptions(nameof(AuthenticateAdminShould_Thow_WhenPasswordIsEmpty));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AuthenticateAdminAsync("test", ""));
            }
        }

        [TestMethod]
        public async Task AuthenticateAdminShould_Thow_WhenUserIsNull()
        {
            var options = DbOptions.GetOptions(nameof(AuthenticateAdminShould_Thow_WhenUserIsNull));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AuthenticateAdminAsync("admin123", "123456"));
            }
        }

        [TestMethod]
        public async Task AuthenticateAdminShould_Throw_WhenCantVerifyPassword()
        {
            var options = DbOptions.GetOptions(nameof(AuthenticateAdminShould_Throw_WhenCantVerifyPassword));

            await UsersDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new UserService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AuthenticateAdminAsync("admin", "123456"));
            }
        }
    }
}
