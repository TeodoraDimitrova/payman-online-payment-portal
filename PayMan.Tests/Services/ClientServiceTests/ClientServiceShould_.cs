﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.ClientServiceTests
{
    [TestClass]
    public class ClientServiceShould_
    {
        [TestMethod]
        public async Task CreateCorrectClient()
        {
            var options = DbOptions.GetOptions(nameof(CreateCorrectClient));

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var client = await sut.CreateClientAsync("Test");

                Assert.IsTrue(client.Name == "Test");
                Assert.IsTrue(client.ClientId == 1);
            }
        }

        [TestMethod]
        public async Task ExistClientAsync_ReturnTrueIfClientNameExists()
        {
            var options = DbOptions.GetOptions(nameof(ExistClientAsync_ReturnTrueIfClientNameExists));

            await ClientDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var isExists = await sut.ExistClientAsync("Test");

                Assert.IsTrue(isExists==true);
            }
        }

        [TestMethod]
        public async Task GetClientsAsync_ReturnClients()
        {
            var options = DbOptions.GetOptions(nameof(GetClientsAsync_ReturnClients));

            await ClientDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var clients = await sut.GetClientsAsync(0,5,"");

                Assert.IsTrue(clients.Count()==2);
            }
        }

        [TestMethod]
        public async Task GetClientsAsync_ReturnSearchClients()
        {
            var options = DbOptions.GetOptions(nameof(GetClientsAsync_ReturnSearchClients));

            await ClientDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var clients = await sut.GetClientsAsync(0, 5, "Test");

                Assert.IsTrue(clients.Count() == 2);
            }
        }


        [TestMethod]
        public async Task GetClientByIdAsync_ReturnCorrectClientDTO()
        {
            var options = DbOptions.GetOptions(nameof(GetClientByIdAsync_ReturnCorrectClientDTO));

            await ClientDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var clients = await sut.GetClientByIdAsync(2);

                Assert.IsTrue(clients.Name == "Test2");
                Assert.IsTrue(clients.ClientId == 2);
            }
        }

        [TestMethod]
        public async Task GetTotalClientsCount_ReturnClientsCount()
        {
            var options = DbOptions.GetOptions(nameof(GetTotalClientsCount_ReturnClientsCount));

            await ClientDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var count =  sut.GetTotalClientsCount();

                Assert.IsTrue(count == 2);
            }
        }


        [TestMethod]
        public async Task GetUsersClientsAsync_ReturnClientsUsers()
        {
            var options = DbOptions.GetOptions(nameof(GetUsersClientsAsync_ReturnClientsUsers));

            await ClientDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var usersClients = await sut.GetUsersClientsAsync(1);

                Assert.IsTrue(usersClients.Count ==1);
            }
        }

        [TestMethod]
        public async Task AddUserToClient_AddUserToClient()
        {
            var options = DbOptions.GetOptions(nameof(AddUserToClient_AddUserToClient));

            await ClientDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var user = await sut.AddUserToClient("bety91",1);

                Assert.IsTrue(user.UserName == "bety91");
            }
        }

        [TestMethod]
        public async Task RemoveUserAsync_RemoveUserFromClient()
        {
            var options = DbOptions.GetOptions(nameof(RemoveUserAsync_RemoveUserFromClient));

            await ClientDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new ClientService(assertContext);
                var user = await sut.RemoveUserAsync(1,1);

                Assert.IsTrue(user.UserName == "didi89");
            }
        }
    }
}
