﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.BannerServiceTests
{
    [TestClass]
    public class UpdateBannerAsyncShould
    {
        //KEEP COMMENTED BECAUSE OF CI/CD

        //[TestMethod]
        //public async Task ReturnCorrectInformationAfterUpdatingBanner()
        //{
        //    var options = DbOptions.GetOptions(nameof(ReturnCorrectInformationAfterUpdatingBanner));

        //    await BannerDatabaseSetUp.ArrangeContextAsync(options);

        //    using (var assertContext = new PayContext(options))
        //    {
        //        var sut = new BannerService(assertContext);
        //        var banner = await sut.UpdateBannerAsync(1, "D:\\", BannerRequestSetUp.Request());

        //        Assert.IsTrue(banner.URL == "www.google.com");
        //        Assert.IsTrue(banner.StartValidity == new DateTime(2019, 05, 05));
        //        Assert.IsTrue(banner.EndValidity == new DateTime(2019, 06, 05));
        //    }
        //}

        [TestMethod]
        public async Task ThrowWhenBannerIsNotInDatabase()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenBannerIsNotInDatabase));

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.UpdateBannerAsync(1, "D:\\", BannerRequestSetUp.Request()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenRootIsNullInUpdate()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenRootIsNullInUpdate));

            await BannerDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.UpdateBannerAsync(1, null, BannerRequestSetUp.Request()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenRequestIsNullInUpdate()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenRequestIsNullInUpdate));

            await BannerDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.UpdateBannerAsync(1, "D:\\", null));
            }
        }
    }
}
