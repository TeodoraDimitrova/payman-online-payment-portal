﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;

namespace PayMan.Tests.Services.BannerServiceTests
{
    [TestClass]
    public class AddBannerAsyncShould
    {
        //KEEP COMMENTED BECAUSE OF CI/CD

        //[TestMethod]
        //public async Task ReturnCorrectBannerInformation()
        //{
        //    var options = DbOptions.GetOptions(nameof(ReturnCorrectBannerInformation));

        //    using (var assertContext = new PayContext(options))
        //    {
        //        var sut = new BannerService(assertContext);
        //        var banner = await sut.AddBannerAsync("D:\\", BannerRequestSetUp.Request());

        //        Assert.IsTrue(banner.URL == "www.google.com");
        //        Assert.IsTrue(banner.StartValidity == new DateTime(2019, 05, 05));
        //        Assert.IsTrue(banner.EndValidity == new DateTime(2019, 06, 05));
        //    }
        //}

        [TestMethod]
        public async Task ThrowWhenRootIsNull()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenRootIsNull));

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AddBannerAsync(null, BannerRequestSetUp.Request()));
            }
        }

        [TestMethod]
        public async Task ThrowWhenRequestIsNull()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenRequestIsNull));

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.AddBannerAsync("D:\\", null));
            }
        }
    }
}
