﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.BannerServiceTests
{
    [TestClass]
    public class GetAllAsyncShould
    {
        [TestMethod]
        public async Task ReturnCorrectBannersCount()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectBannersCount));

            await BannerDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);
                var banners = await sut.GetAllAsync();

                Assert.IsTrue(banners.Count() == 1);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectBannersInfo()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectBannersInfo));

            await BannerDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);
                var banners = await sut.GetAllAsync();
                var banner = banners.FirstOrDefault(x => x.BannerId == 1);

                Assert.IsTrue(banner.BannerId == 1);
                Assert.IsTrue(banner.URL == "www.youtube.com");
                Assert.IsTrue(banner.ImageName == "2bcfe4c5-a918-42f8-a524-018cecc03c92.jpg");
                Assert.IsTrue(banner.StartValidity == new DateTime(2019, 05, 15));
                Assert.IsTrue(banner.EndValidity == new DateTime(2019, 06, 15));
            }
        }
    }
}
