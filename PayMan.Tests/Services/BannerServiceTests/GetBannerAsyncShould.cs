﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.BannerServiceTests
{
    [TestClass]
    public class GetBannerAsyncShould
    {
        [TestMethod]
        public async Task ReturnCorrectBannerInfoWhenAsked()
        {
            var options = DbOptions.GetOptions(nameof(ReturnCorrectBannerInfoWhenAsked));

            await BannerDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);
                var banner = await sut.GetBannerAsync(1);

                Assert.IsTrue(banner.BannerId == 1);
                Assert.IsTrue(banner.URL == "www.youtube.com");
                Assert.IsTrue(banner.ImageName == "2bcfe4c5-a918-42f8-a524-018cecc03c92.jpg");
                Assert.IsTrue(banner.StartValidity == new DateTime(2019, 05, 15));
                Assert.IsTrue(banner.EndValidity == new DateTime(2019, 06, 15));
            }
        }

        [TestMethod]
        public async Task ThrowWhenBannerDoesNotExistInDatabase()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenBannerDoesNotExistInDatabase));

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.GetBannerAsync(1));
            }
        }
    }
}
