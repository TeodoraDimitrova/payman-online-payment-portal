﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.BannerServiceTests
{
    [TestClass]
    public class DeleteBannerAsyncShould
    {
        [TestMethod]
        public async Task DeleteCorrectBanner()
        {
            var options = DbOptions.GetOptions(nameof(DeleteCorrectBanner));

            await BannerDatabaseSetUp.ArrangeContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);
                await sut.DeleteBannerAsync(1, "D:\\");

                Assert.IsTrue(assertContext.Banners.Count() == 0);
            }
        }

        [TestMethod]
        public async Task ThrowWhenBannerDoesNotExist()
        {
            var options = DbOptions.GetOptions(nameof(ThrowWhenBannerDoesNotExist));

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);

                await Assert.ThrowsExceptionAsync<UserException>(() => sut.DeleteBannerAsync(1, "D:\\"));
            }
        }
    }
}
