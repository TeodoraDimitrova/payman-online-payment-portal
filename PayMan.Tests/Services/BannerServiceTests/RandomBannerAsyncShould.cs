﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Data;
using PayMan.Services;
using PayMan.Services.DTOs;
using PayMan.Services.Exceptions;
using PayMan.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Tests.Services.BannerServiceTests
{
    [TestClass]
    public class RandomBannerAsyncShould
    {
        [TestMethod]
        public async Task ReturnsBannerDTO()
        {
            var options = DbOptions.GetOptions(nameof(ReturnsBannerDTO));

            await BannerDatabaseSetUp.ArrangeRandomContextAsync(options);

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);
                var banner = await sut.RandomBannerAsync();

                Assert.AreEqual(banner.GetType(), typeof(BannerDTO));
                Assert.IsTrue(banner.BannerId == 1);
                Assert.IsTrue(banner.ImageName == "2bcfe4c5-a918-42f8-a524-018cecc03c92.jpg");
                Assert.IsTrue(banner.URL == "www.youtube.com");
                Assert.IsTrue(banner.StartValidity == new DateTime(2019, 01, 01));
                Assert.IsTrue(banner.EndValidity == new DateTime(2119, 01, 01));
            }
        }

        [TestMethod]
        public async Task ReturnNullWhenNoBannersAreAvailable()
        {
            var options = DbOptions.GetOptions(nameof(ReturnNullWhenNoBannersAreAvailable));

            using (var assertContext = new PayContext(options))
            {
                var sut = new BannerService(assertContext);
                var banner = await sut.RandomBannerAsync();

                Assert.IsNull(banner);
            }
        }
    }
}
