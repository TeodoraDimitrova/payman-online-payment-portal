﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Web.Areas.User.Controllers;
using PayMan.Web.Models;
using System.Threading.Tasks;

namespace PayMan.Tests.Controllers.UserArea.HomeControllerTests
{
    [TestClass]
    public class PostLogInShould
    {
        [TestMethod]
        public async Task ReturnView()
        {
            var userServiceMock = new Mock<IUserService>();
            var bannerServiceMock = new Mock<IBannerService>();
            bannerServiceMock.Setup(x => x.RandomBannerAsync()).ReturnsAsync(new BannerDTO());
            var model = new HomePageViewModel()
            {
                ImageName = "image.jpg",
                URL = "undefined",
                LogIn = new LoginViewModel()
            };

            var sut = new HomeController(userServiceMock.Object, bannerServiceMock.Object);
            var result = await sut.Login(model);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
