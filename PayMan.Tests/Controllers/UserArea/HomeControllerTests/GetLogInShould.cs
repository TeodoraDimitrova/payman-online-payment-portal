﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Web.Areas.User.Controllers;
using PayMan.Web.Models;
using System.Threading.Tasks;

namespace PayMan.Tests.Controllers.UserArea.HomeControllerTests
{
    [TestClass]
    public class GetLogInShould
    {
        [TestMethod]
        public async Task ReturnViewWhenModelIsNull()
        {
            var userServiceMock = new Mock<IUserService>();
            var bannerServiceMock = new Mock<IBannerService>();
            bannerServiceMock.Setup(x => x.RandomBannerAsync()).ReturnsAsync(new BannerDTO());
            var homePageMock = new HomePageViewModel();

            var sut = new HomeController(userServiceMock.Object, bannerServiceMock.Object);
            var result = await sut.Login();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
