﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayMan.Web.Areas.Admin.Controllers;

namespace PayMan.Tests.Controllers.AdminArea.MenuControllerTests
{
    [TestClass]
    public class IndexShould
    {
        [TestMethod]
        public void ReturnView()
        {
            var sut = new MenuController();
            var result = sut.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
