﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Controllers;

namespace PayMan.Tests.Controllers.AdminArea.ClientControllerTests
{
    [TestClass]
    public class IndexShould
    {
        [TestMethod]
        public void ReturnView()
        {
            var clientServiceMock = new Mock<IClientService>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            var sut = new ClientController(clientServiceMock.Object, accountServiceMock.Object, userServiceMock.Object);
            var result = sut.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
