﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Data.Models;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Controllers;
using PayMan.Web.Areas.Admin.Models;
using PayMan.Web.Models;
using System.Threading.Tasks;

namespace PayMan.Tests.Controllers.AdminArea.ClientControllerTests
{
    [TestClass]
    public class PostAddUserShould
    {
        [TestMethod]
        public async Task ReturnBadRequestWhenStateNotValid()
        {
            var clientServiceMock = new Mock<IClientService>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();
            var model = new ClientDetailsViewModel()
            {
                Client = new ClientViewModel(),
                UserName = "Name",
                Balance = -10
            };

            var sut = new ClientController(clientServiceMock.Object, accountServiceMock.Object, userServiceMock.Object);
            var result = await sut.AddUser(model);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task ReturnBadRequestWhenClientsUsernameNotValid()
        {
            var clientServiceMock = new Mock<IClientService>();
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();
            var model = new ClientDetailsViewModel()
            {
                Client = new ClientViewModel(),
                UserName = "Name",
                Balance = 10
            };

            var sut = new ClientController(clientServiceMock.Object, accountServiceMock.Object, userServiceMock.Object);
            var result = await sut.AddUser(model);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task ReturnOkResult()
        {
            var clientServiceMock = new Mock<IClientService>();
            clientServiceMock.Setup(x => x.AddUserToClient(It.IsAny<string>(), It.IsAny<int>())).ReturnsAsync(new UserApp());
            var userServiceMock = new Mock<IUserService>();
            var accountServiceMock = new Mock<IAccountService>();

            var client = new ClientViewModel()
            {
                ClientId = 1,
                ClientName = "ClientName",
                AccountCount = 1,
                UserCount = 1
            };
            var model = new ClientDetailsViewModel()
            {
                Client = client,
                UserName = "Name",
                Balance = 10
            };

            var sut = new ClientController(clientServiceMock.Object, accountServiceMock.Object, userServiceMock.Object);
            var result = await sut.AddUser(model);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }
    }
}
