﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayMan.Tests.Controllers.AdminArea.ClientControllerTests
{
    [TestClass]
    public class SearchShould
    {
        [TestMethod]
        public async Task ReturnJson()
        {
            var clientServiceMock = new Mock<IClientService>();
            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetUsersNameAsync(It.IsAny<string>())).ReturnsAsync(new List<string>());
            var accountServiceMock = new Mock<IAccountService>();

            var sut = new ClientController(clientServiceMock.Object, accountServiceMock.Object, userServiceMock.Object);
            var result = await sut.Search("text");

            Assert.IsInstanceOfType(result, typeof(JsonResult));
        }
    }
}
