﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Controllers;
using PayMan.Web.Models;

namespace PayMan.Tests.Controllers.AdminArea.BannerControllerTests
{
    [TestClass]
    public class GetAddShould
    {
        [TestMethod]
        public void ReturnView()
        {
            var bannerServiceMock = new Mock<IBannerService>();

            var sut = new BannerController(bannerServiceMock.Object);
            var result = sut.Add();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
