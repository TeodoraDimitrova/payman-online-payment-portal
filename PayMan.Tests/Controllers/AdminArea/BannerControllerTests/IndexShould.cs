﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Web.Areas.Admin.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayMan.Tests.Controllers.AdminArea.BannerControllerTests
{
    [TestClass]
    public class IndexShould
    {
        [TestMethod]
        public async Task ReturnView()
        {
            var bannerServiceMock = new Mock<IBannerService>();
            bannerServiceMock.Setup(x => x.GetAllAsync()).ReturnsAsync(new List<BannerDTO>());

            var sut = new BannerController(bannerServiceMock.Object);
            var result = await sut.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
