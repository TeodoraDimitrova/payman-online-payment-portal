﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Services.Requests;
using PayMan.Web.Areas.Admin.Controllers;
using PayMan.Web.Models;
using System;
using System.Threading.Tasks;

namespace PayMan.Tests.Controllers.AdminArea.BannerControllerTests
{
    [TestClass]
    public class PostAddShould
    {
        [TestMethod]
        public async Task ReturnViewWhenModelStateNotValid()
        {
            var bannerServiceMock = new Mock<IBannerService>();
            bannerServiceMock.Setup(x => x.AddBannerAsync(It.IsAny<string>(), It.IsAny<BannerRequest>())).ReturnsAsync(new BannerDTO());
            var model = new CreateBannerViewModel()
            {
                BannerId = 1,
                URL = "google.com",
                StartValidity = new DateTime(),
                EndValidity = new DateTime(),
                ImageName = "Name",
                Image = new Mock<IFormFile>().Object
            };

            var sut = new BannerController(bannerServiceMock.Object);
            var result = await sut.Add(model);

            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }
    }
}
