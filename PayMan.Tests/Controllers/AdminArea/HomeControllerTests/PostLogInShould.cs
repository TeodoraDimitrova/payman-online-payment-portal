﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Controllers;
using PayMan.Web.Models;
using System.Threading.Tasks;

namespace PayMan.Tests.Controllers.AdminArea.HomeControllerTests
{
    [TestClass]
    public class PostLogInShould
    {
        [TestMethod]
        public async Task ReturnViewWhenModelIsNull()
        {
            var userServiceMock = new Mock<IUserService>();
            var model = new LoginViewModel()
            {
                Username = null,
                Password = null,
                RememberMe = true
            };

            var sut = new HomeController(userServiceMock.Object);
            var result = await sut.Login(model);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
