﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Controllers;
using PayMan.Web.Models;

namespace PayMan.Tests.Controllers.AdminArea.HomeControllerTests
{
    [TestClass]
    public class GetLogInShould
    {
        [TestMethod]
        public void ReturnView()
        {
            var userServiceMock = new Mock<IUserService>();
            var homePageMock = new HomePageViewModel();

            var sut = new HomeController(userServiceMock.Object);
            var result = sut.Login();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
