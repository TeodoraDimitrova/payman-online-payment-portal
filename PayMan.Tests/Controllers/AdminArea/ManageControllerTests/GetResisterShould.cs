﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Controllers;

namespace PayMan.Tests.Controllers.AdminArea.ManageControllerTests
{
    [TestClass]
    public class ResisterShould
    {
        [TestMethod]
        public void ReturnPartialView()
        {
            var userServiceMock = new Mock<IUserService>();

            var sut = new ManageController(userServiceMock.Object);
            var result = sut.Register();

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));
        }        
    }
}
