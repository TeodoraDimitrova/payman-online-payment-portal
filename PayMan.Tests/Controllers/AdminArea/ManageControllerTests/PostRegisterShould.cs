﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Controllers;
using PayMan.Web.Areas.Admin.Models;
using System.Threading.Tasks;

namespace PayMan.Tests.Controllers.AdminArea.ManageControllerTests
{
    [TestClass]
    public class PostRegisterShould
    {
        [TestMethod]
        public async Task ReturnJson()
        {
            var userServiceMock = new Mock<IUserService>();
            var model = new RegisterViewModel()
            {
                FullName = "Ivan Ivanov",
                Username = "Ivancho",
                Password = "newPassword1"
            };

            var sut = new ManageController(userServiceMock.Object);
            var result = await sut.Register(model);

            Assert.IsInstanceOfType(result, typeof(JsonResult));
        }
    }
}
