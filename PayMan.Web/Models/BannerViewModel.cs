﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace PayMan.Web.Models
{
    public abstract class BaseBannerViewModel
    {
        public int BannerId { get; set; }

        public string ImageName { get; set; }
    
        [Required(ErrorMessage = "Please enter URL.")]
        public string URL { get; set; }

        [Required(ErrorMessage = "Please enter this banner's start validity.")]
        public DateTime StartValidity { get; set; }

        [Required(ErrorMessage = "Please enter this banner's end validity.")]
        public DateTime EndValidity { get; set; }
    }

    public class CreateBannerViewModel : BaseBannerViewModel
    {
        [Required(ErrorMessage = "Please enter photo.")]
        public IFormFile Image { get; set; }
    }

    public class EditBannerViewModel : BaseBannerViewModel
    {       
        public IFormFile Image { get; set; }
    }
}
