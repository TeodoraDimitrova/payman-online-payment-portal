﻿using PayMan.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PayMan.Web.Models
{
    public class ClientViewModel
    {
        public int ClientId { get; set; }

        [Required]
        [StringLength(35)]
        public string ClientName { get; set; }

        public int AccountCount { get; set; }

        public int UserCount { get; set; }

        public ICollection<Account> Accounts { get; set; }

        public ICollection<UsersClients> UsersClients { get; set; }

        public IReadOnlyList<AccountViewModel> AccountViewModels { get; set; }
    }
}
