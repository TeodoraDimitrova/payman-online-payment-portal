﻿using PayMan.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PayMan.Web.Models
{
    public class AccountViewModel
    {
        public int AccountId { get; set; }

        public string AccountNumber { get; set; }

        public string Nickname { get; set; }

        [Required]
        public decimal Balance { get; set; }

        public string ClientName { get; set; }

        public ICollection<UsersAccounts> UsersAccounts { get; set; }

        public ICollection<UsersClients> ClientUsers { get; set; }

    }
}
