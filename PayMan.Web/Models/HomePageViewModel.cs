﻿namespace PayMan.Web.Models
{
    public class HomePageViewModel
    {
        public LoginViewModel LogIn { get; set; }

        public string ImageName { get; set; }

        public string URL { get; set; }
    }
}
