﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.User.Models
{
    public class AccountBalanceViewModel
    {
        public decimal Balance { get; set; }

        public string AccountNumber { get; set; }

        public string NickName { get; set; }
    
    }
}
