﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PayMan.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.User.Models
{
    public class TransactionViewModel
    {
        public int TransactionId { get; set; }

        [StringLength(35)]
        [Required(ErrorMessage = "Please enter description.")]
        public string Description { get; set; }

        [Range(0, Double.MaxValue)]
        [Display(Name = "Amount (BGN)")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage = "Provide positive number with given up to 2 digits after the decimal separator, please.")]
        [Required(ErrorMessage = "Please enter correct amount.")]
        public decimal Amount { get; set; }

        public DateTime TimeStamp { get; set; }

        [Display(Name = "Sender's account number")]
        public string SenderAccount { get; set; }

        [Required]
        public int SenderAccountId { get; set; }
        public ICollection<PayAccountViewModel> SenderAccounts { get; set; }

        [Display(Name = "Sender's client")]
        [Required(ErrorMessage = "Please enter correct sender's client.")]
        public string SenderClient { get; set; }

        [Display(Name = "Receiver's account number")]
        [Required(ErrorMessage = "Please enter correct receiver's account.")]
        public string ReceiverAccount { get; set; }

        [Display(Name = "Receiver's client")]
        [Required(ErrorMessage = "Please enter correct receiver's client.")]
        public string ReceiverClient { get; set; }

        public string Movement { get; set; }

        public string Status { get; set; }
    }
}
