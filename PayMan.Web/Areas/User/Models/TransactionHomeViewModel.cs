﻿using PayMan.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.User.Models
{
    public class TransactionHomeViewModel
    {
        public int AccountId { get; set; }

        public ICollection<PayAccountViewModel> SenderAccounts { get; set; }

        public ICollection<PayAccountViewModel> ReceiverAccounts { get; set; }

        public ICollection<PayAccountViewModel> LoggedUserAccounts { get; set; }
    }
}
