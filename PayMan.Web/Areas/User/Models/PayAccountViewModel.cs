﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.User.Models
{
    public class PayAccountViewModel
    {
        public int AccountId { get; set; }

        public string AccountNumber { get; set; }

        public string ClientName { get; set; }
    }
}
