﻿using PayMan.Services.DTOs;
using PayMan.Services.Requests;
using PayMan.Web.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.User.Mappers
{
    public static class PayAccountViewModelMapper
    {
        public static PayAccountViewModel ToPayAccount(this AccountDTO dto)
        {
            var model = new PayAccountViewModel()
            {
                AccountId = dto.AccountId,
                AccountNumber = dto.AccountNumber,
                ClientName = dto.ClientName
            };

            return model;
        }

        public static PayAccountRequest ToRequest(this PayAccountViewModel model)
        {
            if (model is null)
            {
                return null;
            }

            var request = new PayAccountRequest()
            {
                AccountId = model.AccountId,
                AccountNumber = model.AccountNumber
            };

            return request;
        }
    }
}
