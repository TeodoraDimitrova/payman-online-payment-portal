﻿using PayMan.Services.DTOs;
using PayMan.Web.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.User.Mappers
{
    public static class TransactionViewModelMapper
    {
        public static TransactionViewModel MapFrom(this TransactionDTO dto)
        {
            var model = new TransactionViewModel()
            {
                TransactionId = dto.TransactionId,
                SenderAccount = dto.SenderAccount,
                SenderClient = dto.SenderClient,
                ReceiverAccount = dto.ReceiverAccount,
                ReceiverClient = dto.ReceiverClient,
                Description = dto.Description,
                Amount = dto.Amount,
                TimeStamp = dto.TimeStamp,
                Status = dto.Status,
                Movement = dto.Movement
            };

            return model;
        }
    }
}
