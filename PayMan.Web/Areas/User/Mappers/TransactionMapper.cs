﻿using PayMan.Services.Requests;
using PayMan.Web.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.User.Mappers
{
    public static class TransactionMapper
    {
        public static TransactionRequest ToRequest(this TransactionViewModel model)
        {
            return new TransactionRequest()
            {
                SenderAccountId = model.SenderAccountId,
                SenderClient = model.SenderClient,
                ReceiverAccount = model.ReceiverAccount,
                ReceiverClient = model.ReceiverClient,
                Description = model.Description,
                Amount = model.Amount
            };
        }
    }
}
