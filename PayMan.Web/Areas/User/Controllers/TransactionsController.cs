﻿using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PayMan.Services.Contracts;
using PayMan.Services.Exceptions;
using PayMan.Services.Requests;
using PayMan.Web.Areas.User.Mappers;
using PayMan.Web.Areas.User.Models;
using PayMan.Web.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User")]
    public class TransactionsController : Controller
    {
        private readonly ITransactionService transactionService;
        private readonly IAccountService accountService;
        private readonly IMemoryCache cache;
        private static readonly ILog log = LogManager.GetLogger(typeof(TransactionsController));

        public TransactionsController(ITransactionService service, IAccountService accountService, IMemoryCache cache)
        {
            this.transactionService = service ?? throw new ArgumentNullException(nameof(service));
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        [TempData] public string StatusMessage { get; set; }

        public async Task<IActionResult> Index(int id = 0)
        {
            var model = new TransactionHomeViewModel();
            var userId = GetLoggedUserId();

            var senderAccounts = await this.transactionService.SenderAccountNumbersAsync(int.Parse(userId));
            model.SenderAccounts = senderAccounts.Select(x => x.ToPayAccount()).ToList();

            var receiverAccounts = await this.transactionService.ReceiverAccountNumbersAsync(int.Parse(userId));
            model.ReceiverAccounts = receiverAccounts.Select(x => x.ToPayAccount()).ToList();

            model.AccountId = id;
            var loggedUserAccounts = await this.accountService.GetAccountsAsync(int.Parse(userId));
            model.LoggedUserAccounts = loggedUserAccounts.Select(x => x.ToPayAccount()).ToList();

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> MakePayment(int id = 0)
        {
            var model = new TransactionViewModel();
            var accounts = await CacheAccounts();
            model.SenderAccounts = accounts;

            if (id != 0)
            {
                model.SenderAccountId = id;
            }
           
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MakePayment(TransactionViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                TempData["StatusMessage"] = "Error: Please provide valid input";
                var accounts = await CacheAccounts();
                model.SenderAccounts = accounts;
                return View(model);
            }

            try
            {
                var request = model.ToRequest();
                var transaction = await this.transactionService.SendNewTransAsync(request);

                TempData["StatusMessage"] = $"New payment was made successfully";
                return RedirectToAction(nameof(Index), new { id = transaction.TransactionId });
            }
            catch (UserException ex)
            {
                TempData["StatusMessage"] = "Error: " + ex.Message;
                var accounts = await CacheAccounts();
                model.SenderAccounts = accounts;
                return View(model);
            }
            catch (Exception e)
            {
                log.Error("Unexpected exception occured:", e);
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(TransactionViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                TempData["StatusMessage"] = "Error: Please provide valid input";
                var accounts = await CacheAccounts();
                model.SenderAccounts = accounts;
                return View("MakePayment", model);
            }

            try
            {
                var request = model.ToRequest();
                var transaction = await this.transactionService.SaveNewTransAsync(request);

                TempData["StatusMessage"] = $"New transaction was saved successfully";
                return RedirectToAction(nameof(Index), new { id = transaction.TransactionId });
            }
            catch (UserException ex)
            {
                TempData["StatusMessage"] = "Error: " + ex.Message;
                var accounts = await CacheAccounts();
                model.SenderAccounts = accounts;
                return View("MakePayment", model);
            }
            catch (Exception e)
            {
                log.Error("Unexpected exception occured:", e);
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var transaction = await this.transactionService.GetTransactionAsync(id);

            if (transaction is null)
            {
                return NotFound();
            }

            var model = transaction.MapFrom();
            var accounts = await CacheAccounts();
            model.SenderAccounts = accounts;

            var selection = model.SenderAccounts
                    .Where(x => x.AccountNumber == transaction.SenderAccount)
                    .FirstOrDefault();
            model.SenderClient = selection.ClientName;
            model.SenderAccountId = selection.AccountId;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, TransactionViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                TempData["StatusMessage"] = "Error: Please provide valid input";
                var accounts = await CacheAccounts();
                model.SenderAccounts = accounts;
                return View(model);
            }

            try
            {
                if (id != model.TransactionId)
                {
                    TempData["StatusMessage"] = "Error: Please provide valid input";
                    return NotFound();
                }

                var request = model.ToRequest();
                await transactionService.UpdateTransactionAsync(id, request);

                TempData["StatusMessage"] = $"Transaction was successfully modified";
                return RedirectToAction(nameof(Index));
            }
            catch (UserException ex)
            {
                TempData["StatusMessage"] = "Error: " + ex.Message;
                var accounts = await CacheAccounts();
                model.SenderAccounts = accounts;
                return View(model);
            }
            catch (Exception e)
            {
                log.Error("Unexpected exception occured:", e);
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Send(int id, TransactionViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                TempData["StatusMessage"] = "Error: Please provide valid input";
                var accounts = await CacheAccounts();
                model.SenderAccounts = accounts;
                return View("Index", model);
            }

            try
            {
                if (id != model.TransactionId)
                {
                    TempData["StatusMessage"] = "Error: Please provide valid input";
                    return NotFound();
                }

                var request = model.ToRequest();
                var transaction = await this.transactionService.SendSavedAsync(id, request);

                TempData["StatusMessage"] = $"Transaction was sent successfully";
                return RedirectToAction(nameof(Index));
            }
            catch (UserException ex)
            {
                TempData["StatusMessage"] = "Error: " + ex.Message;
                var accounts = await CacheAccounts();
                model.SenderAccounts = accounts;
                return View("Index", model);
            }
            catch (Exception e)
            {
                log.Error("Unexpected exception occured:", e);
                return RedirectToAction("Error", "Home");
            }
        }

        public async Task<IActionResult> SendSaved(int id)
        {
            try
            {
                var transaction = await this.transactionService.SendAsync(id);

                TempData["StatusMessage"] = $"Transaction was sent successfully";
                return RedirectToAction(nameof(Index));
            }
            catch (UserException ex)
            {
                TempData["StatusMessage"] = "Error: " + ex.Message;
                return View("Index");
            }
            catch (Exception e)
            {
                log.Error("Unexpected exception occured:", e);
                return RedirectToAction("Error", "Home");
            }
        }

        public async Task<IActionResult> Details(int id)
        {
            var transaction = await this.transactionService.GetTransactionAsync(id);

            if (transaction == null)
            {
                throw new UserException("Transaction not available");
            }

            var model = transaction.MapFrom();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> GetSentTransactions(PayAccountViewModel senderAccountFilter,
            PayAccountViewModel receiverAccountFilter, PayAccountViewModel loggedUserAccount)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();

            int skip = start != null ? Convert.ToInt32(start) : 0;

            var userId = int.Parse(GetLoggedUserId());
            var request = new FilteredTransactionsRequest()
            {
                UserId = userId,
                SenderAccount = senderAccountFilter.ToRequest(),
                ReceiverAccount = receiverAccountFilter.ToRequest(),
                UserAccount = loggedUserAccount.ToRequest()
            };

            int totalRecord = await this.transactionService.SentTransactionsCountAsync(request);
            var transactions = await this.transactionService.GetTransactionsAsync(skip, 5, request);
            var model = transactions.Select(x => x.MapFrom()).ToList();

            return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });
        }

        [HttpPost]
        public async Task<IActionResult> GetSavedTransactions(PayAccountViewModel senderAccount,
            PayAccountViewModel receiverAccount, PayAccountViewModel loggedUserAccount)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();

            int skip = start != null ? Convert.ToInt32(start) : 0;

            var userId = int.Parse(GetLoggedUserId());
            var request = new FilteredTransactionsRequest()
            {
                UserId = userId,
                SenderAccount = senderAccount.ToRequest(),
                ReceiverAccount = receiverAccount.ToRequest(),
                UserAccount = loggedUserAccount.ToRequest()
            };


            int totalRecord = await this.transactionService.SavedTransactionsCountAsync(request);
            var transactions = await this.transactionService.GetSavedTransactionsAsync(skip, 5, request);
            var model = transactions.Select(x => x.MapFrom()).ToList();

            return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });
        }

        [HttpPost]
        public async Task<IActionResult> Complete(string accountNumber)
        {
            var accounts = await this.transactionService.SearchAsync(accountNumber);

            return Json(accounts.OrderBy(x => x.AccountNumber).Take(5));
        }

        private async Task<ICollection<PayAccountViewModel>> CacheAccounts()
        {
            var userId = GetLoggedUserId();

            var cashedAccounts = await cache.GetOrCreateAsync<ICollection<PayAccountViewModel>>("Accounts", async (cacheEntry) =>
            {
                cacheEntry.SlidingExpiration = TimeSpan.FromMinutes(1);
                var accounts = (await this.accountService.GetAccountsAsync(int.Parse(userId)))
                                                .Select(x => x.ToPayAccount())
                                                .ToList();
                return accounts;
            });

            return cashedAccounts;
        }

        private string GetLoggedUserId()
        {
            if (User == null)
            {
                throw new ArgumentException(nameof(User));
            }

            return User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        }
    }
}