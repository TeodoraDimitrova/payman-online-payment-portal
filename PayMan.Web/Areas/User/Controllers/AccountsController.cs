﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.User.Models;
using PayMan.Web.Models;

namespace PayMan.Web.Areas.User.Controllers
{

    [Area("User")]
    [Authorize(Roles = "User")]
    public class AccountsController : Controller
    {

        private readonly IAccountService accountService;
        private readonly IUserService userService;

        public AccountsController(IAccountService accountService, IUserService userService)
        {
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        public async Task<IActionResult> Index()
        {
            var userId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var accounts = await this.accountService.GetUserAccountsAsync(userId);
            var model = new AccountViewModel();
            model.UsersAccounts = accounts;

            return View(model);
        }

        public async Task<IActionResult> GetAccountsBalance()
        {
            var userId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var accounts = await this.accountService.GetUserAccountsAsync(userId);

            ICollection<AccountBalanceViewModel> model = accounts.Select(x => new AccountBalanceViewModel()
            {
                AccountNumber = x.Account.AccountNumber,
                Balance = x.Account.Balance,
                NickName = x.Nickname
            }).ToList();

            return Json(model);
        }

        [HttpGet]
        public IActionResult EditNickname(int id)
        {
            var model = new AccountBalanceViewModel();

            return PartialView("_EditNicknamePartial", model);

        }

        [HttpPost]
        public async Task<IActionResult> EditNickname(int id, string name)
        {
            var userId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var account = await this.accountService.EditNickNameAsync(id, userId, name);

            return RedirectToAction("Index");
        }
    }
}