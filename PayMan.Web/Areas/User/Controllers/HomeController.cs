﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PayMan.Services.Contracts;
using PayMan.Web.Models;

namespace PayMan.Web.Areas.User.Controllers
{
    [Area("User")]
    [Authorize(Roles = "User")]
    public class HomeController : Controller
    {
        private readonly IUserService userService;
        private readonly IBannerService bannerService;

        public HomeController(IUserService userService, IBannerService bannerService)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.bannerService = bannerService ?? throw new ArgumentNullException(nameof(bannerService));
        }

        [TempData] public string StatusMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            var model = new HomePageViewModel();
            var banner = await this.bannerService.RandomBannerAsync();

            if (banner != null)
            {
                model.ImageName = banner.ImageName;
                model.URL = banner.URL;
            }

            return View("Index", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(HomePageViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var banner = await this.bannerService.RandomBannerAsync();

            if (banner != null)
            {
                model.ImageName = banner.ImageName;
                model.URL = banner.URL;
            }

            if (ModelState.IsValid)
            {
                var user = await this.userService.AuthenticateAsync(model.LogIn.Username, model.LogIn.Password);

                if (user == null)
                {
                    ModelState.AddModelError("", "username is not valid");
                    return View("Index", model);
                }

                var scheme = CookieAuthenticationDefaults.AuthenticationScheme;

                var principal = this.userService.CreateClaimsPrincipal(user, scheme);

                var coockieOptions = this.userService.GetCookieOptions();

                await HttpContext.SignInAsync(scheme, principal, coockieOptions);

                return RedirectToAction("Index", "Accounts");
            }
            TempData["StatusMessage"] = "Error: Invalid log in details";
            return View("Index", model);
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login", "Home");
        }

    }
}