﻿using PayMan.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.Admin.Models
{
    public class ClientDetailsViewModel
    {
        public ClientViewModel Client { get; set; }

        public string UserName { get; set; }

        [Range(0, Double.MaxValue)]
        [Display(Name = "Amount (BGN)")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage = "Provide positive number with given up to 2 digits after the decimal separator, please.")]
        public decimal Balance { get; set; }

    }
}
