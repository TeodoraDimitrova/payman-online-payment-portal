﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PayMan.Services.Contracts;
using PayMan.Web.Models;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.Admin.Controllers
{

    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        private readonly IUserService userService;

        public HomeController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        [AllowAnonymous]
        [ResponseCache(Location = ResponseCacheLocation.Client, Duration = 86400)]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View("Index");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (ModelState.IsValid)
            {

                var user = await this.userService.AuthenticateAdminAsync(model.Username, model.Password);

                if (user == null)
                {
                    ModelState.AddModelError("", "username is not valid");
                    return View("Index", model);
                }

                var scheme = CookieAuthenticationDefaults.AuthenticationScheme;

                var principal = this.userService.AdminClaimsPrincipal(user, scheme);

                var coockieOptions = this.userService.GetCookieOptions();

                await HttpContext.SignInAsync(scheme, principal, coockieOptions);

                return RedirectToAction("Index", "Menu");
            }

            return View("Index", model);
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login", "Home");
        }
    }
}
