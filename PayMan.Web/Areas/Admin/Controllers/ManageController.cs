﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PayMan.Data.Models;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Models;
using PayMan.Web.Models;

namespace PayMan.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ManageController : Controller
    {
        private readonly IUserService userService;

        public ManageController(IUserService userService)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        [TempData] public string StatusMessage { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AllUsers()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = await this.userService.GetTotalUsersCountAsync();
            var users = await this.userService.GetUsersAsync(skip, pageSize, searchValue);

            return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = users });
        }

        [HttpGet]
        public ActionResult Register()
        {
            var model = new RegisterViewModel();

            return PartialView("_RegisterUserPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            if (this.ModelState.IsValid)
            {
                if (await userService.ExistUserAsync(model.Username))
                {
                    return BadRequest("Username already exists!");
                }

                var user = new UserApp { UserName = model.Username };
                var result = await this.userService.CreateAsync(model.Username, model.FullName, model.Password);
                return Json(result);
            }

            return PartialView("_RegisterUserPartial", model);
        }
    }
}