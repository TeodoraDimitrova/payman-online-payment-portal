﻿using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayMan.Services.Contracts;
using PayMan.Services.Exceptions;
using PayMan.Web.Mappers;
using PayMan.Web.Models;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class BannerController : Controller
    {
        private readonly IBannerService bannerService;
        private static readonly ILog log = LogManager.GetLogger(typeof(BannerController));

        public BannerController(IBannerService service)
        {
            this.bannerService = service ?? throw new ArgumentNullException(nameof(service));
        }

        [TempData]
        public string StatusMessage { get; set; }

        public async Task<IActionResult> Index()
        {
            var banners = await this.bannerService.GetAllAsync();
            var models = banners.Select(x => x.MapFrom()).ToList();

            return View(models);
        }

        [HttpGet]
        public IActionResult Add()
        {
            var model = new CreateBannerViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(CreateBannerViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                TempData["StatusMessage"] = "Error: Please provide valid input";
                return View(model);
            }

            try
            {
                if (model.Image != null)
                {
                    if (!IsValidImage(model.Image))
                    {
                        TempData["StatusMessage"] = "Error: Please provide a .jpg or .png file smaller than 2MB";
                        return View(model);
                    }
                }

                var request = model.CreateRequest();
                var banner = await this.bannerService.AddBannerAsync(GetUploadsRoot(), request);

                TempData["StatusMessage"] = $"New banner was created successfully";
                return RedirectToAction(nameof(Index), new { id = banner.BannerId });
            }
            catch (UserException ex)
            {
                TempData["StatusMessage"] = "Error: " + ex.Message;
                return View(model);
            }
            catch (Exception e)
            {
                log.Error("Unexpected exception occured:", e);
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var banner = await this.bannerService.GetBannerAsync(id);

            if (banner is null)
            {
                return NotFound();
            }

            var model = banner.MapFrom();
            
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditBannerViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                TempData["StatusMessage"] = "Error: Please provide valid input";
                return View(model);
            }

            try
            {
                if (model.Image != null)
                {
                    if (!IsValidImage(model.Image))
                    {
                        TempData["StatusMessage"] = "Error: Please provide a .jpg or .png file smaller than 2MB";
                        return View(model);
                    }
                }

                if (id != model.BannerId)
                {
                    TempData["StatusMessage"] = "Error: Please provide valid input";
                    return NotFound();
                }

                var request = model.EditRequest();
                await bannerService.UpdateBannerAsync(id, GetUploadsRoot(), request);

                TempData["StatusMessage"] = "Banner was successfully modified";

                return RedirectToAction(nameof(Index));
            }
            catch (UserException ex)
            {
                TempData["StatusMessage"] = "Error: " + ex.Message;
                return View(model);
            }
            catch (Exception e)
            {
                log.Error("Unexpected exception occured:", e);
                return RedirectToAction("Error", "Home");
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            var banner = await bannerService.GetBannerAsync(id);

            if (banner == null)
            {
                return NotFound();
            }

            var model = banner.MapFrom();

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await bannerService.DeleteBannerAsync(id, GetUploadsRoot());

            TempData["StatusMessage"] = $"Banner was successfully deleted";

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Details(int id)
        {
            var banner = await this.bannerService.GetBannerAsync(id);

            if (banner == null)
            {
                throw new UserException("Banner not available");
            }

            var model = banner.MapFrom();

            return View(model);
        }

        private string GetUploadsRoot()
        {
            var environment = HttpContext.RequestServices
                .GetService(typeof(IHostingEnvironment)) as IHostingEnvironment;

            return Path.Combine(environment.WebRootPath, "img", "banners");
        }

        private bool IsValidImage(IFormFile image)
        {
            var type = image.ContentType;
            if (type != "image/png" && type != "image/jpg" && type != "image/jpeg")
            {
                return false;
            }

            return image.Length <= 2048 * 2048;
        }
    }
}