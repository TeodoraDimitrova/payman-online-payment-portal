﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PayMan.Data.Models;
using PayMan.Services.Contracts;
using PayMan.Services.DTOs;
using PayMan.Web.Areas.Admin.Models;
using PayMan.Web.Mappers;
using PayMan.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ClientController : Controller
    {
        private readonly IClientService clientService;
        private readonly IAccountService accountService;
        private readonly IUserService userService;


        public ClientController(IClientService clientService, IAccountService accountService, IUserService userService)
        {
            this.clientService = clientService;
            this.accountService = accountService;
            this.userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AllClients()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            int totalRecord = this.clientService.GetTotalClientsCount();
            var clients = await this.clientService.GetClientsAsync(skip, pageSize, searchValue);
            var model = clients.Select(x => x.MapFrom()).ToList();

            return Json(new { draw = draw, recordsFiltered = totalRecord, recordsTotal = totalRecord, data = model });
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClientViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return BadRequest("Please submit valid form!");
            }

            try
            {
                if (await clientService.ExistClientAsync(model.ClientName))
                {
                    return BadRequest("Client name already exists!");
                }
                var client = await this.clientService.CreateClientAsync(model.ClientName);
                var clientView = client.MapFrom();
                return Json(clientView);
            }
            catch (Exception ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }
        }

        public async Task<IActionResult> Details(int id)
        {
            var client = await this.clientService.GetClientByIdAsync(id);

            if (client == null)
            {
                return RedirectToAction("Error","Home");
            }

            var accounts = await this.accountService.GetClientAccountsAsync(id);

            if (accounts==null)
            {
                return BadRequest("There is no accounts!");
            }

            var users = await this.clientService.GetUsersClientsAsync(id);
            if (users == null)
            {
                return BadRequest("There is no users!");
            }
            client.Accounts = accounts;
            client.UsersClients = users;

            var model = new ClientDetailsViewModel();
            model.Client = client.MapFrom();
            var accountViewModels = accounts.Select(a => a.MapFrom()).ToList();
            model.Client.AccountViewModels = accountViewModels;
            return View(model);
        }

        public async Task<IActionResult> ClientUsers(int id)
        {
            var client = await this.clientService.GetClientByIdAsync(id);
            var usersClient = await this.clientService.GetUsersClientsAsync(id);
            client.UsersClients = usersClient;

            var viewModel = new ClientDetailsViewModel();
            viewModel.Client = client.MapFrom();
            return PartialView("_ClientPartial", viewModel);
        }


        [HttpGet]
        public IActionResult AddUser()
        {
            return View("Details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUser(ClientDetailsViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return BadRequest("Please submit valid form!");
            }

            try
            {
                var user = await this.clientService.AddUserToClient(model.UserName, model.Client.ClientId);

                return Ok($"User \"{user.UserName}\" was added succesfully!");
            }
            catch (Exception)
            {
                return BadRequest("This user can not be added! Please check username!");
            }
        }

        public async Task<IActionResult> RemoveUser(int id,int clientId)
        {
            var client = await this.clientService.GetClientByIdAsync(clientId);
            if (client==null)
            {
                return BadRequest("Client does not exist");
            }
            try
            {
                var user = await clientService.RemoveUserAsync(id, clientId);

                return Ok();
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }

        public async Task<IActionResult> Search(string term)
        {
            var names = await this.userService.GetUsersNameAsync(term);
            return Json(names);
        }
    }
}
