﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PayMan.Services.Contracts;
using PayMan.Web.Areas.Admin.Models;
using PayMan.Web.Mappers;
using PayMan.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayMan.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class AccountsController : Controller
    {
        private readonly IAccountService accountService;
        private readonly IClientService clientService;

        public AccountsController(IAccountService accountService, IClientService clientService)
        {
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new ClientDetailsViewModel();

            return View("Details", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClientDetailsViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return BadRequest("Please submit valid form!");
            }
            try
            {
                decimal minSum = 20;
                if (model.Balance < minSum)
                {
                    return BadRequest("Please enter a correct sum!");
                }
                var result = await this.accountService.CreareAccountAsync(model.Balance, model.Client.ClientId);

                return Ok($"Account was created succesfully!");
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }

        public async Task<IActionResult> ClientAccounts(int id)
        {
            var client = await this.clientService.GetClientByIdAsync(id);
            var accounts = await this.accountService.GetClientAccountsAsync(id);
            client.Accounts = accounts;

            var viewModel = new ClientDetailsViewModel();
            viewModel.Client = client.MapFrom();
            return PartialView("_AccountPartial", viewModel);
        }

        public async Task<ActionResult> Details(int id)
        {
            var model = new AccountViewModel();

            model.UsersAccounts = await this.accountService.GetAccountUsersAsync(id);

            return PartialView("_AccountInfoPartial", model);
        }

        public async Task<ActionResult> ClientUsersDetails(int id, int accId)
        {
            var model = new AccountViewModel();

            model.AccountId = accId;
            model.ClientUsers = await this.clientService.GetUsersClientsAsync(id);

            return PartialView("_ClientUsersInfo", model);
        }

        public async Task<IActionResult> AddUser(int id, int accId)
        {

            try
            {
                var user = await this.accountService.AddAccountToUserAsync(accId, id);


                return Ok($"User \"{user.User.UserName}\" was added to account!");
            }
            catch (Exception)
            {
                return BadRequest("User can not be added to account!");
            }
        }

        public async Task<IActionResult> RemoveUser(int id, int accId)
        {

            try
            {
                var user = await this.accountService.RemoveAccountFromUserAsync(accId, id);

                return Ok($"User \"{user.User.UserName}\" was removed from account!");
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home");
            }
        }
    }
}
