﻿using PayMan.Services.DTOs;
using PayMan.Web.Models;
//using System.Linq;

namespace PayMan.Web.Mappers
{
    public static class ClientViewModelMapper
    {
        public static ClientViewModel MapFrom(this ClientDTO dto)
        {
            return new ClientViewModel()
            {
                ClientId=dto.ClientId,
                ClientName = dto.Name,
                Accounts = dto.Accounts,
                UsersClients = dto.UsersClients,
            };
        }
    }
}
