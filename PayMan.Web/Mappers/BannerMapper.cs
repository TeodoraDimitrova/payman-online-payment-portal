﻿using PayMan.Services.Requests;
using PayMan.Web.Models;

namespace PayMan.Web.Mappers
{
    public static class BannerMapper
    {
        public static BannerRequest CreateRequest(this CreateBannerViewModel entity)
        {
            return new BannerRequest
            {
                Image = entity.Image,
                ImageName = entity.ImageName,
                URL = entity.URL,
                StartValidity = entity.StartValidity,
                EndValidity = entity.EndValidity
            };
        }

        public static BannerRequest EditRequest(this EditBannerViewModel entity)
        {
            return new BannerRequest
            {
                Image = entity.Image,
                ImageName = entity.ImageName,
                URL = entity.URL,
                StartValidity = entity.StartValidity,
                EndValidity = entity.EndValidity
            };
        }
    }
}
