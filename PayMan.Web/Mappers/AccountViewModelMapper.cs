﻿using PayMan.Data.Models;
using PayMan.Web.Models;

namespace PayMan.Web.Mappers
{
    public static class AccountViewModelMapper
    {
        public static AccountViewModel MapFrom(this Account dto)
        {
            return new AccountViewModel()
            {
                AccountId=dto.AccountId,
                AccountNumber = dto.AccountNumber,
                Balance = dto.Balance
            };
        }
    }
}
