﻿using PayMan.Services.DTOs;
using PayMan.Web.Models;

namespace PayMan.Web.Mappers
{
    public static class BannerViewModelMapper
    {
        public static EditBannerViewModel MapFrom(this BannerDTO dto)
        {
            if (dto is null)
            {
                return null;
            }

            var model = new EditBannerViewModel()
            {
                BannerId = dto.BannerId,
                ImageName = dto.ImageName,
                URL = dto.URL,
                StartValidity = dto.StartValidity,
                EndValidity = dto.EndValidity
            };

            return model;
        }
    }
}
