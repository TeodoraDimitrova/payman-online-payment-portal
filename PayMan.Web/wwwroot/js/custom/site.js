﻿
//register user
$(function () {
    var placeholderElement = $('#modal-placeholder');

    $('button[data-toggle="ajax-modal"]').click(function (ev) {
        var url = $(this).data('url');
        $.get(url).done(function (data) {
            placeholderElement.html(data);
            placeholderElement.find('.modal').modal('show');
        });
    });

    placeholderElement.on('click', '[data-save="modal"]', function (event) {
        event.preventDefault();

        var form = $(this).parents('.modal').find('form');
        var actionUrl = form.attr('action');
        var dataToSend = form.serialize();

        $.post(actionUrl, dataToSend).done(function (data) {
            var newBody = $('.modal-body', data);
            placeholderElement.find('.modal-body').replaceWith(newBody);
            var isValid = placeholderElement.find('[name="IsValid"]').val() == 'False';

            if (!isValid) {
                placeholderElement.find('.modal').modal('hide');
                toastr.options = {
                    "positionClass": "toast-top-center",
                }
                toastr.success('User was created');
            }

        }).fail(function (failResponse) {
            toastr.options = {
                "positionClass": "toast-top-center",
            }
            toastr.error(failResponse.responseText);
        });;

    });
});

//create new client
$('#create-client-form').submit(function (ev) {
    ev.preventDefault();

    var $this = $(this);
    var url = $this.attr('action');
    var dataToSend = $this.serialize();

    $.post(url, dataToSend, function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-center",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.success('Client ' + response.ClientName + ' was sreated');

    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-center",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);
    });
});

//add user to client
$('#add-user-form').submit(function (event) {
    event.preventDefault();

    var $this = $(this);
    var url = "/Admin/Client/AddUser";
    var inputs = $this.find('input');
    var serializedInput = inputs.serialize();

    $.post(url, serializedInput, function (response) {
        toastr.options = {
            "positionClass": "toast-top-center",
        }

        toastr.success(response);

        var id = $this.attr('clientId');

        $.get("/Admin/Client/ClientUsers/" + id).done(function (data) {
            $('#user-clients-table').empty();

            $('#user-clients-table').append(data);
        })
            .fail(function (failResponse) {
                toastr.options = {
                    "positionClass": "toast-top-center",
                }
                toastr.error(failResponse.responseText);
            });

    }).fail(function (response) {
        toastr.options = {
            "positionClass": "toast-top-center",
        }
        toastr.error(response.responseText);
    });
});

//remove user from client
$('#user-clients-table').on('click', 'a', (function (ev) {
    ev.preventDefault();

    var $this = $(ev.target)

    var id = $this.attr('userId');
    var clientId = $this.attr('clientId');

    $.ajax(swal({
        title: 'Are you sure you would like to remove user?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, send it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: true,
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "/Admin/Client/RemoveUser/" + id + '?clientId=' + clientId,
                type: "POST",
                success: () => {
                    swal(
                        'Removed!',
                        'The user was removed',
                        'success').then(function () {
                            $.get("/Admin/Client/ClientUsers/" + clientId).done(function (data) {
                                $('#user-clients-table').empty();
                                $('#user-clients-table').append(data);

                            })
                        });

                }
            })
        }
        else if (result.dismiss === swal.DismissReason.cancel) {
            swal(
                'Cancelled',
                'The user was not removed!',
                'error'
            )
        }
    })
    );
}));

//add account to client
$('#add-account-form').submit(function (event) {
    event.preventDefault();

    var $this = $(this);
    var url = "/Admin/Accounts/Create";
    var inputs = $this.find('input');
    var serializedInput = inputs.serialize();

    $.post(url, serializedInput, function (response) {
        toastr.options = {
            "positionClass": "toast-top-center",
        }

        toastr.success(response);

        var id = $this.attr('clientId');

        $.get("/Admin/Accounts/ClientAccounts/" + id).done(function (data) {
            $('#client-accounts-table').empty();

            $('#client-accounts-table').append(data);
        })
            .fail(function (failResponse) {
                toastr.options = {
                    "positionClass": "toast-top-center",
                }
                toastr.error(failResponse.responseText);
            });

    }).fail(function (response) {
        toastr.options = {
            "positionClass": "toast-top-center",
        }
        toastr.error(response.responseText);
    });
});

//remove user from account
$(function () {

    var placeholderElement = $('#modal-acc-placeholder');

    $('#client-accounts-table').on('click', 'button[data-toggle="acc-details-modal"]', function (ev) {

        var $this = $(ev.target);

        var id = $this.attr('acc-id');

        var url = "/Admin/Accounts/Details/" + id;

        $.get(url).done(function (data) {
            placeholderElement.html(data);
            placeholderElement.find('.modal').modal('show');
        });
    });

    placeholderElement.on('click', 'button[data-toggle="remove-user"]', (function (ev) {
        ev.preventDefault();

        var $this = $(ev.target)
        var accountId = $this.attr('accountId')
        var id = $this.attr('userId')
        debugger
        var url = "/Admin/Accounts/RemoveUser/" + id + '?accId=' + accountId;

        $.get(url).done(function (response) {
            toastr.options = {
                "positionClass": "toast-top-center",
            }

            toastr.success(response)
        }).fail(function (response) {
            toastr.options = {
                "positionClass": "toast-top-center",
            }
            toastr.error(response.responseText);
        });
    }));

});

//add user to account
$(function () {

    var placeholderElement = $('#modal-acc-add-user');

    $('#client-accounts-table').on('click', 'button[data-toggle="add-user-modal"]', function (ev) {

        var $this = $(ev.target);

        var id = $this.attr('client-id');
        const accId = $this.attr('acc-id')

        var url = "/Admin/Accounts/ClientUsersDetails/" + id + '?accId=' + accId;

        $.get(url).done(function (data) {
            placeholderElement.html(data);
            placeholderElement.find('.modal').modal('show');
        });
    });

    placeholderElement.on('click', 'button[data-toggle="add-user"]', (function (ev) {
        ev.preventDefault();

        var $this = $(ev.target)
        var accountId = $this.attr('accountId')
        var id = $this.attr('userId')

        var url = "/Admin/Accounts/AddUser/" + id + '?accId=' + accountId;

        $.get(url).done(function (response) {
            toastr.options = {
                "positionClass": "toast-top-center",
            }

            toastr.success(response)
        }).fail(function (response) {
            toastr.options = {
                "positionClass": "toast-top-center",
            }
            toastr.error(response.responseText);
        });
    }));

});

$('#userTbl').on('click', 'button[data-toggle="edit-name"]', function (ev) {
    ev.preventDefault();

    var $this = $(ev.target)

    var id = $this.attr('accId');

    $.ajax(swal({
        title: 'Input new nickname',
        input: 'text',
        inputPlaceholder: 'Enter new nickname',
        //inputValue: inputValue,
        showCancelButton: true,
       // inputValidator: (value)
    }).then((result) => {
        if (!result.value) {
            //return 'You need to write something!'
            swal(
                'Pleace enter nickname',
            )
        } else
        {
            $.ajax({
                url: "/User/Accounts/EditNickname/" + id + '?name=' + result.value,
                type: "POST",
            })

        }
    }))
})

