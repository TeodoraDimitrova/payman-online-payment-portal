﻿$(() => {
    const previewImageElement = $('.picture-img-preview');

    const showImagePreview = (event) => {
        const file = event.target.files && event.target.files[0];

        if (file) {
            const reader = new FileReader();
            reader.onload = (readEvent) => {
                previewImageElement
                    .attr('src', readEvent.target.result);
            };
            reader.readAsDataURL(file);
        }
    };

    // register events
    $('input[type="file"]').change(showImagePreview);
});