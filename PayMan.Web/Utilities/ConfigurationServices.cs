﻿using Microsoft.Extensions.DependencyInjection;
using PayMan.Services;
using PayMan.Services.Contracts;

namespace PayMan.Web.Utilities
{
    public static class ConfigurationServices
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IBannerService, BannerService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IUserService, UserService>();

            return services;
        }
    }
}
